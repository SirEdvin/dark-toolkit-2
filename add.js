const fs = require('fs');

const packageName = process.argv[2];
const fileName = process.argv[3];

const packageConfiguration = JSON.parse(fs.readFileSync("package.json", "utf-8"))
packageConfiguration.scripts[`build:${fileName}`] = `better-npm-run build:${fileName}`
packageConfiguration.scripts[`upload:${fileName}`] = `better-npm-run upload:${fileName}`
packageConfiguration.betterScripts[`build:${fileName}`] = `node ./../../build.js ${fileName}.ts`
packageConfiguration.betterScripts[`upload:${fileName}`] = `node ./../../upload.js ${packageName} ${fileName}.lua`
packageConfiguration.nx.targets[`build:${fileName}`] = {
    "dependsOn": [
        "^build"
    ]
}
packageConfiguration.nx.targets[`upload:${fileName}`] = {
    "dependsOn": [
        `build:${fileName}`
    ]
}


fs.writeFileSync("package.json", JSON.stringify(packageConfiguration, null, 2))