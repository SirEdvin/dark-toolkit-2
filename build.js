const fs = require('fs');
const { exec } = require('child_process');
const { error } = require('console');

const fileName = process.argv[2];
const clearFileName = fileName.replace(".ts", "")
const tempConfigFileName = `tsconfig_${clearFileName}_temp.json`

const commonBuildConfiguration = JSON.parse(fs.readFileSync("tsconfig.json", "utf-8"))
commonBuildConfiguration.include = commonBuildConfiguration.include.map(x => x == "*.ts" ? fileName : x)

fs.writeFileSync(tempConfigFileName, JSON.stringify(commonBuildConfiguration))
exec(`tstl -p ${tempConfigFileName} --luaBundle ${clearFileName}.lua --luaBundleEntry ${fileName}`, (error, stdout, stderr) => {
    console.log(stderr)
    console.log(stdout)
    fs.unlinkSync(tempConfigFileName)
})