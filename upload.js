const fs = require('fs');
const path = require('path');
const axios = require('axios');

const apiKey = process.env.LITTLE_HAND_KEY || 'your-api-key-here';
const fileName = process.argv[3];
const groupName = process.argv[2];
const url = `https://d1.siredvin.site/storage/${groupName}/${fileName.replace(".lua", "")}`; // The U

if (!fileName) {
    console.error('Please provide a file name as a command argument.');
    process.exit(1);
}

// Check if the file exists
const filePath = path.resolve(fileName);
if (!fs.existsSync(filePath)) {
    console.error(`File "${fileName}" does not exist.`);
    process.exit(1);
}

// Prepare the request headers and file
const headers = {
    'x-api-key': apiKey,
    'Content-Type': 'application/octet-stream' // This tells the server we're sending binary data
};

// Read the file as a stream
const fileStream = fs.createReadStream(filePath);

// Send the POST request with the file data
axios.post(url, fileStream, { headers })
    .then(response => {
        console.log('File uploaded successfully:', response.data);
    })
    .catch(error => {
        console.error('Error uploading file:', error.response ? error.response.data : error.message);
    });
