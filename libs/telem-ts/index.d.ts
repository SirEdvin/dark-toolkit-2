export declare class InputAdapter {
}

export declare class OutputAdapter {
}

export declare class Backplane {
    public addInput(name: string, input: InputAdapter): Backplane
    public cache(value: boolean)
    public debug(value: boolean)
    public addOutput(name: string, output: OutputAdapter): Backplane
    public cycleEvery(seconds: number)
    public cycle(): Backplane
}

export declare class Metric {

}

export declare type CustomMetricProvider1 = () => {[key: string]: number}
export declare type CustomMetricProvider2 = () => LuaMultiReturn<Metric[]>

/** @noSelf **/
export declare class StandartInputs {
    public helloWorld(value: number): InputAdapter
    public itemStorage(peripheralID: string): InputAdapter
    public fluidStorage(peripheralID: string): InputAdapter
    public custom(merticProvider: CustomMetricProvider1 | CustomMetricProvider2) : InputAdapter
}

/** @noSelf **/
export declare class PlotterOutput {
    public line(win: Window, filter: string, bg: Color, fg: Color, maxEntries?: number): OutputAdapter
}

/** @noSelf **/
export declare class BasaltOutput {
    public graph(monitor: any, filter: string, bg: Color, fg: Color, maxEntries?: number): OutputAdapter
}

/** @noSelf **/
export declare class StandartOutputs {
    public helloWorld(): OutputAdapter
    public grafana(endpoint: string, apiKey: string): OutputAdapter
    public plotter: PlotterOutput
    public basalt: BasaltOutput
}

export declare class MetricConfiguration {
    name: string
    value: number
    unit: string
    source: string
}

export declare const input: StandartInputs
export declare const output: StandartOutputs
/** @noSelf **/
export declare function metric(name: string, value: number): Metric
/** @noSelf **/
export declare function metric(config: MetricConfiguration): Metric

export declare function backplane(): Backplane
