declare type Direction = "up" | "down"
declare type Result = LuaMultiReturn<[boolean, string | null]>
declare type TResult<Type> = LuaMultiReturn<[Type | null, string | null]>
declare type InteractionMode = "both" | "entity" | "block"

declare type Cube = {
    x1: number;
    x2: number;
    y1: number;
    y2: number;
    z1: number;
    z2: number;
    tint?: number;
    texture?: string;
}