export declare class MonitorGroup {
    [key: number]: string[]
}

export declare class Monitor{
    setMonitor(name: string)
    setMonitorGroup(group: MonitorGroup)
}

/** @noSelf **/
export declare function addMonitor(): Monitor

/** @noSelf **/
export declare function autoUpdate(value?: boolean)


/** @noSelf **/
export declare function stopUpdate()