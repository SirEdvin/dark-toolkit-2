import {CLIApplication, Subcomand} from "cc-corelib/argparse"
import * as logging from "cc-corelib/logging"
import { pretty_print } from "cc.pretty"
import { ScanApiMods } from "typed-peripheral/apis/scan"
import { UniversalScanner, universalScannerProvider } from "typed-peripheral/unlimitedperipheralworks/universalScanner"

const _log = logging.create("upw_scanner")

class UpwScanner extends CLIApplication {
    constructor() {
        super("upw_scanner", [
            new Subcomand("scansend", "upw_scanner scansend [-d]", {
                "d": "debug"
            }),
            new Subcomand("tscansend", "upw_scanner tscansend [-d]", {
                "d": "debug"
            }),
        ])
    }

    itemProcessor(itemResult: Array<LuaTable>) {
        itemResult.forEach(v => {
            if (v.has("tags") && (v.get("tags") as Array<string>).length == 0) {
                v.set("tags", new LuaTable())
            }
            if (v.has("itemGroups") && (v.get("itemGroups") as Array<string>).length == 0) {
                v.set("itemGroups", new LuaTable())
            }
        })
    }


    scanSomething(scanner: UniversalScanner, operationName: string, type: ScanApiMods, radius: number) {
        _log.info("Scan ", type)
        let [result, err] = scanner.scan(type, radius)
        if (err != null) {
            _log.info("Exception on scanning ", type, ": ", err)
        } else if (result.length == 0) {
            _log.info("Scanning result are empty, ignoring it")
        } else {
            if (type == "item") {
                this.itemProcessor(result)
            }
            let [sucess, r] = pcall(textutils.serialiseJSON, result)
            if (!sucess) {
                pretty_print(result)
                throw r
            }
            let url = "https://d1.siredvin.site/storage/data/" + type
            let apiKey = settings.get("dark_toolkit.api_key")
            if (apiKey == null) {
                throw "Please, define API key in settings"
            }
            let headers = new LuaTable<string, string>()
            headers["x-api-key"] = apiKey
            let response = http.post(url, r, headers, true)
            if (!response[0]) {
                _log.warn("Cannot send ", type, " failed with reason ", response[1])
            } else {
                let httpResponse = response[0] as HTTPResponse
                _log.info("Scan result for ", type, " are sent")
                httpResponse.close()
            }
        }
        let sleepTime = (scanner.getCooldown(operationName) / 1000) + 0.1
        _log.info("Sleeping for ", sleepTime, " until next scan")
        os.sleep(sleepTime)
    }

    scansend(args, flags) {
        let scanner = universalScannerProvider.findOrThrow()
        let operationName = scanner.getOperations()[0]
        this.scanSomething(scanner, operationName, "item", 4)
        this.scanSomething(scanner, operationName, "block", 1)
        this.scanSomething(scanner, operationName, "entity", null)
        this.scanSomething(scanner, operationName, "player", 4)
    }

    tscansend(args, flag) {
        let scanner = universalScannerProvider.findOrThrow()
        let operationName = scanner.getOperations()[0]
        this.scanSomething(scanner, operationName, "xp", null)
    }
}

new UpwScanner().run(...$vararg)