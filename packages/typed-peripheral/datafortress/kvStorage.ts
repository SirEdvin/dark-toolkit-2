import { IPeripheralProvider } from '../base'

/** @noSelf **/
export interface KVStorage extends IPeripheral {
    delete(key: String)
    put(key: String, value: String, expire?: number | null)
    get(key: String): String | null
    list(): Array<String>
    get_ex(key: String): number | null
    put_ex(key: String, expire?: number | null)
}

export const kvStoragePeripheralProvider = new IPeripheralProvider<KVStorage>("kv_storage", () => null)
