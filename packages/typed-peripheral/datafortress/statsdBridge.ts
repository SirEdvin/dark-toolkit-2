import * as logging from 'cc-corelib/logging'
import { IPeripheralProvider } from '../base'

const _log = logging.create("tperipheral.statsd_bridge")

/** @noSelf **/
export interface StatsDBridge extends IPeripheral {
    count(aspect: String, delta: number)
    delta(aspect: String, delta: number)
    gauge(aspect: String, value: number)
    set(aspect: String, eventName: String)
    time(aspect: String, timeInMs: number)
}

/** @noSelf **/
export class DummyStatsDBridge implements StatsDBridge {
    count(aspect: String, delta: number) {}
    delta(aspect: String, delta: number) {}
    gauge(aspect: String, value: number) {}
    set(aspect: String, eventName: String) {}
    time(aspect: String, timeInMs: number) {}
}

export const statsDBridgeProvider = new IPeripheralProvider<StatsDBridge>("statsd_bridge", () => new DummyStatsDBridge())
