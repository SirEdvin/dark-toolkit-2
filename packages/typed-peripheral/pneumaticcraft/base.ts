/** @noSelf **/
export interface CommonPneumaticCraftPeripheral extends IPeripheral {
    /* direction only make sense for vacuum pump */
    getPressure(direction?: string): number
    getDangerPressure(): number
    getCriticalPressure(): number
}

export interface HeatPneumaticCraftPeripheral extends IPeripheral {
    /* direction only make sense for vortex tube */
    getTemperature(direction?: string): number
}