import { IPeripheralProvider } from '../base'
import { CommonPneumaticCraftPeripheral } from './base'

/** @noSelf **/
export interface UniversalSensor extends CommonPneumaticCraftPeripheral {
    getSensorNames(): LuaTable<string>
    getSensor(): string
    setSensor(sensor?: string | number)
    setTextField(text: string)
    getTextField(): string
    isSensorEventBased(): boolean
    getSensorValue(): number
    getMinWorkingPressure(): number
    setGPSToolCoordinate(slot: number, x: number, y: number, z: number)
}

export const universalSensorProvider = new IPeripheralProvider<UniversalSensor>("pneumaticcraft:universal_sensor", () => null)
