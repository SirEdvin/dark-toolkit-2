import { IPeripheralProvider } from '../../base'
import { DummyFuelApi, FuelApi } from '../../apis/fuel'
import { InteractionApi } from '../../apis/interaction'

/** @noSelf **/
export declare interface ProtectiveAutomata extends FuelApi, InteractionApi {
}

/** @noSelf **/
class DummyProtectiveAutomata extends DummyFuelApi implements ProtectiveAutomata {
    use(mode: InteractionMode, direction?: Direction): Result {
        return $multi(true, null)
    }
    swing(mode: InteractionMode, direction?: Direction): Result {
        return $multi(true, null)
    }
    chisel(mode: 'block', target: string, direction?: Direction): Result {
        return $multi(true, null)
    }
}

export const protectiveProvider = new IPeripheralProvider<ProtectiveAutomata>("protectiveAutomata", () => new DummyProtectiveAutomata())