import { IPeripheralProvider } from '../../base'
import { DummyFuelApi, FuelApi } from '../../apis/fuel'

/** @noSelf **/
export declare interface MasonAutomata extends FuelApi {
    chisel(mode: "block", target: string, direction?: Direction): Result
}

/** @noSelf **/
class DummyMasonAutomata extends DummyFuelApi implements MasonAutomata {
    chisel(mode: 'block', target: string, direction?: Direction): Result {
        return $multi(true, null)
    }
}

export const masonProvider = new IPeripheralProvider<MasonAutomata>("masonAutomata", () => new DummyMasonAutomata())