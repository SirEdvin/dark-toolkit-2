import { IPeripheralProvider, ItemDetail, ShortItemDetail } from '../base'

export type ItemQuery = string | {
    nbt?: string
    tag?: string
    displayName?: string
    name?: string
}

/** @noSelf **/
export declare interface ItemStorageAPI extends IPeripheral {
    items(): LuaTable<number, ItemDetail>
    items(detailed: true): LuaTable<number, ItemDetail>
    items(detailed: false): LuaTable<number, ShortItemDetail>
    pushItem(toName: string, itemQuery?: string, limit?: number): number
    pullItem(fromName: string, itemQuery?: string, limit?: number): number
}

export const itemStoragePeripheralProvider = new IPeripheralProvider<ItemStorageAPI>("item_storage", () => null)
