import { FluidDetail, IPeripheralProvider } from '../base'

/** @noSelf **/
export declare interface FluidStorageAPI extends IPeripheral {
    tanks(): LuaTable<Number, FluidDetail>
    pushFluid(toName: string, limit?: number, fluid?: string): number
    pullFluid(fromName: string, limit?: number, fluid?: string): number
}

export const fluidStoragePeripheralProvider = new IPeripheralProvider<FluidStorageAPI>("fluid_storage", () => null)
