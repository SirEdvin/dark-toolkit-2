/** @noSelf **/
export declare interface ConfigurationAPI extends IPeripheral {
    getConfiguration(): LuaTable
}
