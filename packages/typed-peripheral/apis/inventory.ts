import { IPeripheralProvider, ItemDetail, ShortItemDetail } from '../base'

/** @noSelf **/
export declare interface InventoryAPI extends IPeripheral {
    size(): number
    list(): LuaTable<number, ShortItemDetail>
    getItemDetail(slot: number): ItemDetail
    getItemLimit(slot: number): number
    pushItems(toName: string, fromSlot: number, limit?: number, toSlot?: number): number
    pullItems(fromName: string, fromSlot: number, limit?: number, toSlot?: number): number
}

export const inventoryPeripheralProvider = new IPeripheralProvider<InventoryAPI>("inventory", () => null)
