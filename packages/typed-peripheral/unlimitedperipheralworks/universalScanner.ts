import { ConfigurationAPI } from "../apis/configuration";
import { OperationApi } from "../apis/operations";
import { ScanApi} from "../apis/scan";
import { IPeripheralProvider } from "../base";

/** @noSelf **/
export interface UniversalScanner extends ScanApi, OperationApi, ConfigurationAPI {

}

export const universalScannerProvider = new IPeripheralProvider<UniversalScanner>("universal_scanner", () => null)