import { Computer } from "./computer";
import { IPeripheralProvider } from "../base";

/** @noSelf **/
export interface Turtle extends Computer {
}

export const turtleProvider = new IPeripheralProvider<Turtle>("turtle", () => null)
