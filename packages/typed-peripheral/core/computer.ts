import { IPeripheralProvider } from '../base'

/** @noSelf **/
export interface Computer extends IPeripheral {
    turnOn()
    shutdown()
    reboot()
    getID(): number
    isOn(): boolean
    getLabel(): string
}

export const computerProvider = new IPeripheralProvider<Computer>("computer", () => null)
