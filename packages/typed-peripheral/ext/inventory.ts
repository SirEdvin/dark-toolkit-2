import { InventoryAPI } from "../apis/inventory";
import { NamePredicate, ItemPredicate } from "../base"

export function extractPredicate(name: string | NamePredicate): NamePredicate {
    return type(name) == "function" ? name as NamePredicate : (itemName: string) => name == itemName
}

export function searchItemByName(inv: InventoryAPI, name: string | NamePredicate): number {
    const internalPredicate = extractPredicate(name)
    let invList = inv.list();
    if (invList == null) {
        return -1;
    }
    for (const [slot, item] of invList) {
        if (internalPredicate(item.name)) {
            return slot as number
        }
    }
    return -1;
}

export function searchItemByPredicate(inv: InventoryAPI, predicate: ItemPredicate): number {
    let invList = inv.list();
    if (invList == null) {
        return -1;
    }
    for (const [slot, item] of invList) {
        if (predicate(inv.getItemDetail(slot))) {
            return slot as number
        }
    }
    return -1;
}