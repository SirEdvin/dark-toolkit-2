export type NamePredicate = (name: string) => boolean;

export type ShortItemDetail = {
    count: number
    name: string
}

export type ItemDetail = {
    count: number
    maxCount: number
    displayName: string
    name: string
    tags: {
        [key: string]: boolean
    }
}

export type FluidDetail = {
    amount: number
    capacity: number
    name: string
}

export type ItemPredicate = (info: ItemDetail) => boolean
export type FluidPredicate = (info: FluidDetail) => boolean
export type CombinedPredicate = (info: ItemDetail | FluidDetail) => boolean
export type PeripheralPredicate = (info: IPeripheral) => boolean

export abstract class PeripheralDiscovery<T extends IPeripheral> {
    abstract discover(): T[]
}

export class PeripheralDiscoveryByName<T extends IPeripheral> extends PeripheralDiscovery<T> {
    constructor(
        readonly provider: IPeripheralProvider<T>,
        readonly name: string
    ) {
        super()

    }
    discover(): T[] {
        return [this.provider.wrap(this.name)]
    }
}

export class PeripheralDiscoveryByTypePattern<T extends IPeripheral> extends PeripheralDiscovery<T> {
    constructor(
        readonly pattern: string
    ) {
        super()

    }
    discover(): T[] {
        const peripheralNames = peripheral.getNames()
        const purifiedPattern = "^" + string.gsub(string.gsub(this.pattern, "*", ".*")[0], "-", "%%-")[0] + "$"
    
        return peripheralNames.filter(peripheralName => {
            const peripheralTypes = peripheral.getType(peripheralName)
            const matchedType = peripheralTypes.find(value => {
                let result = string.find(value, purifiedPattern)
                return result[0] != null
            })
            return matchedType !== undefined
        }).map(peripheralName => peripheral.wrap(peripheralName)) as T[]
    }
}

export class IPeripheralProvider<Type extends IPeripheral> {
    constructor(readonly peripheral_type: string, readonly dummy_constructor: () => Type | null = () => null) {}

    find(derrefed_type?: string): Type | null {
        let seached_type = derrefed_type == null ? this.peripheral_type : derrefed_type
        const any_peripheral = peripheral.find(seached_type)[0]
        if (any_peripheral != null) {
            return any_peripheral as Type
        }
        if (settings.get("dark_toolkit.allow_dummy", "True") == "True") {
            return this.dummy_constructor()
        }
        return null
    }

    searchByType(pattern: string, derrefed_type?: string): Array<Type>{
        const targetedPeripherals = this.collect(derrefed_type)
        const purifiedPattern = "^" + string.gsub(string.gsub(pattern, "*", ".*")[0], "-", "%%-")[0] + "$"
        if (targetedPeripherals.length == 0)
            return
        return targetedPeripherals.filter(p => {
            const peripheralTypes = peripheral.getType(p)
            const matchedType = peripheralTypes.find(value => {
                let result = string.find(value, purifiedPattern)
                return result[0] != null
            })
            print("peripheral", peripheral.getName(p), "is", matchedType, "for", purifiedPattern)
            return matchedType !== undefined
        })
    }

    findOrThrow(derrefed_type?: string): Type {
        let per = this.find(derrefed_type)
        if (per == null) { throw "Cannot find peripheral " + (derrefed_type || this.peripheral_type) }
        return per
    }

    searchOneByType(pattern: string, derrefed_type?: string): Type {
        let targetedPeripherals = this.searchByType(pattern, derrefed_type)
        if (targetedPeripherals.length == 0) {
            throw "Cannot find peripheral " + (derrefed_type || this.peripheral_type)
        }
        if (targetedPeripherals.length > 1) {
            const names = targetedPeripherals.map(p => peripheral.getName(p)).join(";")
            throw "Too many peripherals was found " + (derrefed_type || this.peripheral_type) + " even with pattern " + pattern + " this peripherals was found: " + names
        }
        return targetedPeripherals[0]
    }
    
    collect(derrefed_type?: string): Array<Type> {
        let seached_type = derrefed_type == null ? this.peripheral_type : derrefed_type
        const any_peripherals = peripheral.find(seached_type)
        if (any_peripherals.length == 0) return []
        return any_peripherals.map((v) => v as Type)
    }
    
    wrap(name: string): Type | null {
        const wrapped = peripheral.wrap(name)
        if (wrapped == null)
            return null
        return wrapped as Type
    }
}
