import {CLIApplication, Subcomand} from "cc-corelib/argparse"
import * as fetcher from "cc-corelib/fetcher"

class DarkApplication extends CLIApplication {
    constructor() {
        super("dark", [
            new Subcomand("pumpup", "dark pumpup", {}),
            new Subcomand("update", "dark update", {"-r": "reboot"}),
            new Subcomand("install", "dark install [program name]", {})
        ]);
    }

    pumpup() {
        shell.run("wget", "https://raw.githubusercontent.com/SquidDev-CC/mbs/master/mbs.lua", "mbs.lua")
        shell.run("wget", "https://cloud-catcher.squiddev.cc/cloud.lua")
        shell.run("mbs.lua", "install")
        os.reboot()
    }

    update(args, flags) {
        fetcher.installEverything()
        if (flags["reboot"]) {
            os.reboot()
        }
    }
    install(args, flags) {
        const currentPrograms = settings.get(fetcher.EXTRA_PROGRAMS_SETTING)
        if (currentPrograms == null) {
            settings.set(fetcher.EXTRA_PROGRAMS_SETTING, args[1])
        } else {
            settings.set(fetcher.EXTRA_PROGRAMS_SETTING, currentPrograms + "," + args[1])
        }
        this.update(null, {"reboot": true})
    }
}

new DarkApplication().run(...$vararg)
