import {CLIApplication, Subcomand} from "cc-corelib/argparse"
import { statueWorkbenchProvider } from "typed-peripheral/unlimitedperipheralworks/statueWorkbench"
import * as pretty from "cc.pretty";

class DDebug extends CLIApplication {
    constructor() {
        super("ddebug", [
            new Subcomand("event", "ddebug event", {}),
            new Subcomand("shape", "ddebug shape 2|4|8|16", {})
        ])
    }

    event(args, flags) {
        while (true) {
            let event_data = os.pullEvent()
            pretty.pretty_print(event_data)
        }
    }

    shape(args: Array<String>, flags) {
        let workbench = statueWorkbenchProvider.findOrThrow()
        let size = tonumber(args[0])
        let cube: Cube = {
            x1: 0,
            y1: 0,
            z1: 0,
            x2: size,
            y2: size,
            z2: size
        }
        workbench.setCubes([cube])
    }
}

new DDebug().run(...$vararg)