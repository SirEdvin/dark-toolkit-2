import {CLIApplication, Subcomand} from "cc-corelib/argparse"

class DRun extends CLIApplication {
    constructor() {
        super("drun", [
            new Subcomand("pr", "drun pr [program name]", {}),
            new Subcomand("test", "drun test [program name]", {})
        ])
    }

    pr(args: Array<String>, flags) {
        shell.run("wget", "run", "https://d1.siredvin.site/storage/playground/" + args[0] + "/latest")
    }

    test(args: Array<String>, flags) {
        shell.run("wget", "run", "https://d1.siredvin.site/storage/tests/" + args[0] + "/latest")
    }
}

new DRun().run(...$vararg)