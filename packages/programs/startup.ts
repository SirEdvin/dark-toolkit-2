// Settings alias for startup
fs.list("dark_toolkit/programs").forEach((program) => {
    let programName = program.replace(".lua", "")
    shell.setAlias(programName, fs.combine("/dark_toolkit/programs", program))
})

print("Tweaked with Dark Toolkit")

// Running storing configurarion is present

if (fs.exists("/dark_toolkit/main.lua")) {
    shell.run("/dark_toolkit/main.lua")
}