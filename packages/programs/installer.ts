import * as fetcher from "cc-corelib/fetcher";

function setGroupAndLabel(...args: string[]) {
  settings.set("dark_toolkit.allow_dummy_peripheral", "False");
  // if (args.length >= 1 && args[0] != "main") {
  //     settings.set(fetcher.GROUP_SETTING, args[0])
  //     let extraPrograms = args.slice(1).join(",")
  //     if (extraPrograms.length > 0) {
  //         settings.set(fetcher.EXTRA_PROGRAMS_SETTING, extraPrograms);
  //     }
  // }
  settings.save();
}

setGroupAndLabel(...$vararg);
fetcher.installEverything();
os.reboot();
