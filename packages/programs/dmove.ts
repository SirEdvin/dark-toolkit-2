import {CLIApplication, Subcomand} from "cc-corelib/argparse"
import { inventoryPeripheralProvider, InventoryAPI }  from "typed-peripheral/apis/inventory"
import * as logging from "cc-corelib/logging"
import { NamePredicate } from "typed-peripheral/base"

const _log = logging.create("dmove")

class DMove extends CLIApplication {
    constructor() {
        super("dmove", [
            new Subcomand("item", "dmove item <input_filter> <output_filter> <optional_item_filter> (-d)", {"-d": "debug"}),
        ])
    }

    private purifyCondition(condition: string | null): string | null {
        if (condition == null) return null
        return "^" + string.gsub(string.gsub(condition, "*", ".*")[0], "-", "%%-")[0] + "$"
    }

    private buildPredicate(condition: string | null): NamePredicate {
        if (condition == null) return (name: string) => true
        return (name: string) => {
            let result = string.find(name, condition)
            return result[0] != null
        }
    }

    private prepare(args, flags): [InventoryAPI[], string[], NamePredicate] {
        let purifiedInputsFilter = this.purifyCondition(args[1])
        let purifiedOutputsFilter = this.purifyCondition(args[2])
        let purifiedItemFilter = this.purifyCondition(args[3])
        let inputFilter = this.buildPredicate(purifiedInputsFilter)
        let outputFilter = this.buildPredicate(purifiedOutputsFilter)
        let itemFilter = this.buildPredicate(purifiedItemFilter)
        let inventories = inventoryPeripheralProvider.collect()
        let inputs = inventories.filter(p => inputFilter(peripheral.getName(p)))
        let outputs = inventories.map(p => peripheral.getName(p)).filter(p => outputFilter(p))
        if (flags.debug) {
            _log.info("Detected as an inputs with next filter", purifiedInputsFilter)
            inputs.forEach(value => _log.info("\t", peripheral.getName(value)))
            _log.info("Detected as outputs with next filter", purifiedOutputsFilter)
            outputs.forEach(value => _log.info("\t", value))
            if (purifiedItemFilter == null) {
                _log.info("Item filter is not detected")
            } else {
                _log.info("Detected item filter", purifiedItemFilter)
            }
        }
        return [inputs, outputs, itemFilter]
    }

    item(args, flags) {
        let [inputs, outputs, itemFilter] = this.prepare(args, flags)
        if (!flags.debug) {
            inputs.forEach((value) => {
                _log.info("Processing input", peripheral.getName(value))
                let items = value.list()
                if (items != null) {
                    for (const [slot, item] of items) {
                        if (itemFilter(item.name)) {
                            let pushedAmount = 0
                            for (let output of outputs) {
                                let currentPushedAmounts = value.pushItems(output, slot as number)
                                pushedAmount += currentPushedAmounts
                                while (pushedAmount < item.count && currentPushedAmounts > 0) {
                                    let currentPushedAmounts = value.pushItems(output, slot as number)
                                    pushedAmount += currentPushedAmounts
                                }
                                if (pushedAmount >= item.count) break
                            }
                        }
                    }
                }
            })
        }
    }
}

new DMove().run(...$vararg)