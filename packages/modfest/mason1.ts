import { MasonAgent } from './shared/mason'
import * as doerlib from "cc-doerlib"

let agency = new doerlib.Agency()
agency.configure()
agency.addAgent(new MasonAgent("mason1", "minecraft:stone", "minecraft:stone_bricks"))
agency.run()