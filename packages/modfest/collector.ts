import * as doerlib from "cc-doerlib"
import * as logging from "cc-corelib/logging"
import * as bus from "cc-corelib/bus"
import { LavaBucket, lavaBucketProvider } from "typed-peripheral/turtlematic/mics/lava_bucket"
import { Chatter, chatterProvider } from "typed-peripheral/turtlematic/mics/chatter"
import { ProtectiveAutomata, protectiveProvider } from "typed-peripheral/turtlematic/automata/protective"
import { FactoryEvent } from './shared/factory_event'

const _log = logging.create("pusher")


class ScoreTable {
    amount: number

    constructor(amount: number) {
        this.amount = amount
    }
}


class CollectorAgent extends doerlib.BaseAgent {
    lavaBucket: LavaBucket
    chatter: Chatter
    protectiveAutomata: ProtectiveAutomata
    scoreTable: ScoreTable
    fileName: string
    cleaningCounter: number
    cleaningCounterThreshold

    constructor(fileName: string = "score.r") {
        super("collector", 1)
        this.scoreTable = new ScoreTable(0)
        this.fileName = fileName
        this.cleaningCounter = 0
        this.cleaningCounterThreshold = 60
    }

    private draw() {
        this.chatter.setMessage("Already " + this.scoreTable.amount + " blocks produced")
    }

    private readyForNext() {
        bus.queueEvent("pusher", new FactoryEvent(this.name, true), true)
    }

    private save() {
        let f = fs.open(this.fileName, "w")
        if (f[0] == null) {
            _log.error("Cannot open file for saving results")
        } else {
            let handler = f[0] as FileHandle
            handler.write(textutils.serialise(this.scoreTable))
            handler.close()
        }
    }

    private load() {
        let f = fs.open(this.fileName, "r")
        if (f[0] == null) {
            this.scoreTable = new ScoreTable(0)
        } else {
            let handler = f[0] as FileHandle
            this.scoreTable = textutils.unserialise(handler.readAll())
            handler.close()
        }
    }

    protected configureInternal(): void {
        this.load()
        this.lavaBucket = lavaBucketProvider.findOrThrow()
        this.chatter = chatterProvider.findOrThrow()
        this.protectiveAutomata = protectiveProvider.findOrThrow()
        this.protectiveAutomata.setFuelConsumptionRate(6)
        this.draw()
    }

    private cleanInventory() {
        this.cleaningCounter += 1
        if (this.cleaningCounter >= this.cleaningCounterThreshold) {
            for (const slot of $range(2, 16)) {
                let itemCount = turtle.getItemCount(slot)
                if (itemCount >0) {
                    turtle.select(slot)
                    this.lavaBucket.void()
                }
            }
            turtle.select(1)
            this.cleaningCounter = 0
        }
    }

    protected perform(): void {
        let [present, info] = turtle.inspect()
        if (present && info["name"] == "minecraft:chiseled_stone_bricks") {
            let [result, err] = this.protectiveAutomata.swing("block")
            if (!result) {
                _log.error("Cannot dig block:", err)
            } else {
                this.readyForNext()
                this.scoreTable.amount += 1
                this.save()
                this.draw()
            }   
        } else if (!present) {
            this.readyForNext()
        }
        this.cleanInventory()
    }
}

let agency = new doerlib.Agency()
agency.configure()
agency.addAgent(new CollectorAgent())
agency.run()