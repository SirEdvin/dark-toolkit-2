import * as doerlib from "cc-doerlib"
import * as logging from "cc-corelib/logging"
import { pistonProvider, Piston } from "typed-peripheral/turtlematic/mics/piston"
import { FactoryEvent, FACTORY_EVENT } from './shared/factory_event'

const _log = logging.create("pusher")

class PusherAgent extends doerlib.BaseAgent {
    requiredParts: Map<string, boolean>
    pendingPush: boolean = false
    piston: Piston

    constructor() {
        super("pusher", 0.2)
        this.requiredParts = new Map()
        this.requiredParts.set("mason1", false)
        this.requiredParts.set("mason2", false)
        this.requiredParts.set("smith", false)
        this.requiredParts.set("collector", false)
    }

    protected markAndCheck(event: FactoryEvent) {
        if (event.isReady) {
            if (this.requiredParts.get(event.partName) == null) {
                _log.warn("Cannot find part with name", event.partName)
            } else if (!this.requiredParts.get(event.partName)) {
                this.requiredParts.set(event.partName, true)
                _log.info("Received part info", event.partName)
                let leftCounter = 0
                for (let value of this.requiredParts.values()) {
                    if (!value) {
                        leftCounter += 1
                    }
                }
                if (leftCounter == 0) {
                    this.pendingPush = true
                } else {
                    _log.info("Waiting for more parts:", leftCounter)
                }
            }
        }
    }

    protected configureInternal(): void {
        this.piston = pistonProvider.findOrThrow()
        this.piston.setSilent(true)
        let self = this
        this.subscribeToEvent(FACTORY_EVENT, function(event: FactoryEvent) { self.markAndCheck(event) })
    }

    protected perform(): void {
        if (this.pendingPush) {
            let present = turtle.inspect()[0]
            if (present) {
                let [result, err] = this.piston.push()
                if (result) {
                    for (let key of this.requiredParts.keys()) {
                        this.requiredParts.set(key, false)
                    }
                    this.pendingPush = false
                    _log.info("Block pushed successful")
                } else {
                    _log.warn("Cannot push block:", err)
                }
            } else {
                _log.warn("Waiting for block to push")
            }
        }
    }
}

let agency = new doerlib.Agency()
agency.configure()
agency.addAgent(new PusherAgent())
agency.run()