import * as events from "cc-corelib/event"

export const FACTORY_EVENT = "factory"


export class FactoryEvent implements events.IEvent {

    constructor(readonly partName: string, readonly isReady: boolean) {}

    get_name(): string {
        return FACTORY_EVENT
    }
    get_args(): any[] {
        return [this.partName, this.isReady]
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != FACTORY_EVENT) return null;
        let ev = new FactoryEvent(args[1], args[2]);
        return ev;
    }
}

events.eventInitializers.push(FactoryEvent.init)
