import { TransformAgent } from './transormer'
import { smithingProvider, SmithingAutomata  } from 'typed-peripheral/turtlematic/automata/smithing'

export class SmithingAgent extends TransformAgent<SmithingAutomata> {
    protected operation(): Result {
        return this.peripheral.smelt("block")
    }
    constructor(name: string, startingBlock: string, resultBlock: string) {
        super(name, smithingProvider, startingBlock, resultBlock)
    }
}