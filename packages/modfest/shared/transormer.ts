import * as doerlib from "cc-doerlib"
import * as logging from "cc-corelib/logging"
import * as bus from "cc-corelib/bus"
import { IPeripheralProvider } from "typed-peripheral/base"
import { FuelApi } from "typed-peripheral/apis/fuel"
import { FactoryEvent } from './factory_event'

const _log = logging.create("transformer")


export abstract class TransformAgent<Type extends FuelApi> extends doerlib.BaseAgent {
    provider: IPeripheralProvider<Type>
    peripheral: Type
    startingBlock: string
    resultBlock: string

    constructor(name: string, provider: IPeripheralProvider<Type>, startingBlock: string, resultBlock: string) {
        super(name, 1)
        this.provider = provider
        this.startingBlock = startingBlock
        this.resultBlock = resultBlock
    }

    protected configureInternal(): void {
        this.peripheral = this.provider.findOrThrow()
        this.peripheral.setFuelConsumptionRate(6)
    }

    protected readyForNext() {
        bus.queueEvent("pusher", new FactoryEvent(this.name, true), true)
    }

    protected abstract operation(): Result

    protected perform(): void {
        let [result, info] = turtle.inspect()
        if (result) {
            if (info["name"] == this.startingBlock) {
                let [result, err] = this.operation()
                if (result) {
                    this.readyForNext()
                } else {
                    _log.error("Problem with operation", err)
                }
            } else if (info["name"] == this.resultBlock) {
                this.readyForNext()
            } else {
                _log.error("Very strange block!")
            }
        } else {
            _log.info("Cannot find any block, send ready for next")
            this.readyForNext()
        }
    }
}