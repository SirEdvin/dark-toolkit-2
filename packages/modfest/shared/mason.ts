import { TransformAgent } from './transormer'
import { masonProvider, MasonAutomata  } from 'typed-peripheral/turtlematic/automata/mason'

export class MasonAgent extends TransformAgent<MasonAutomata> {
    protected operation(): Result {
        return this.peripheral.chisel("block", this.resultBlock)
    }
    constructor(name: string, startingBlock: string, resultBlock: string) {
        super(name, masonProvider, startingBlock, resultBlock)
    }
}