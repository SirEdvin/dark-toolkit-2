import { MasonAgent } from './shared/mason'
import * as doerlib from "cc-doerlib"

let agency = new doerlib.Agency()
agency.configure()
agency.addAgent(new MasonAgent("mason2", "minecraft:stone_bricks", "minecraft:chiseled_stone_bricks"))
agency.run()