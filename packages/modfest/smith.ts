import { SmithingAgent } from './shared/smithing'
import * as doerlib from "cc-doerlib"

let agency = new doerlib.Agency()
agency.configure()
agency.addAgent(new SmithingAgent("smith", "minecraft:cobblestone", "minecraft:stone"))
agency.run()