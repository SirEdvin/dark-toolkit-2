import * as events from "cc-corelib/event"

export const LUMBER_REPORT = "lumber_report"


export class LumberReportEvent implements events.IEvent {

    constructor(readonly lumberName: string, readonly needRestock: boolean, readonly slotsToClean: Array<number>) {}

    get_name(): string {
        return LUMBER_REPORT
    }
    get_args(): any[] {
        return [this.lumberName, this.needRestock, this.slotsToClean]
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != LUMBER_REPORT) return null;
        let ev = new LumberReportEvent(args[1], args[2], args[3]);
        return ev;
    }
}

events.eventInitializers.push(LumberReportEvent.init)
