import * as events from "cc-corelib/event"

export const DRAGON_IS_NOT_HERE = "dragon_is_not_here"


export class DragonIsNotHereEvent implements events.IEvent {

    constructor() {}

    get_name(): string {
        return DRAGON_IS_NOT_HERE
    }
    get_args(): any[] {
        return []
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != DRAGON_IS_NOT_HERE) return null;
        let ev = new DragonIsNotHereEvent();
        return ev;
    }
}

events.eventInitializers.push(DragonIsNotHereEvent.init)
