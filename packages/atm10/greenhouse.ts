import * as doerlib from "cc-doerlib";
import { CollectorAgent } from "cc-doerlib/extra/collectors";
import { COLLECTION_REPORT_EVENT } from "cc-doerlib/extra/events";
import { RepublishingAgent } from "cc-doerlib/extra/republisher"
import { DictionarySyncAgent } from "cc-doerlib/extra/dictionary"

let ae2Interfaces = peripheral.find("extendedae:ex_interface");

if (ae2Interfaces.length == 0) {
    throw "Cannot find ae2 interface";
}

let ae2InterfaceName = peripheral.getName(ae2Interfaces[0])

function regularOutputStrategy(name: string) {
    return ae2InterfaceName
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new CollectorAgent(
        os.getComputerLabel().concat("_collector"),
        20,
        regularOutputStrategy,
        (name: string) => name.startsWith("sophisticated")
    )
);
agency.addAgent(
    new RepublishingAgent(
        os.getComputerLabel(),
        [COLLECTION_REPORT_EVENT],
        "greenhouse_observer"
    )
)
agency.addAgent(
    new DictionarySyncAgent()
)
agency.run();
