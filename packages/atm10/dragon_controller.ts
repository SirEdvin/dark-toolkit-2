import * as doerlib from "cc-doerlib";
import { RedstoneEvent } from "cc-corelib/event"
import { DRAGON_IS_NOT_HERE } from "./shared/dragon_events";
import { InventoryAPI, inventoryPeripheralProvider } from "typed-peripheral/apis/inventory";
import { searchItem } from "typed-peripheral/ext/inventory";

export class DragonController extends doerlib.BaseAgent {

    router: InventoryAPI
    chest: InventoryAPI
    lastPlacedTime: number
    delayBetweenPlaces: number

    constructor(name: string, delayBetweenPlaces: number = 60) {
        super(name, 0);
        this.lastPlacedTime = 0
        this.delayBetweenPlaces = delayBetweenPlaces
    }

    protected moveCrystalas(event?: any) {
        let currentTime = os.epoch("utc")  / 1000
        if ((currentTime - this.lastPlacedTime) < this.delayBetweenPlaces) {
            this.log.info("Ignore placing crystal, because of limits")
            return
        }
        let targetSlot = searchItem(this.chest, "minecraft:end_crystal", (info => {
            return info.get("count") >= 4
        }))
        if (targetSlot == -1) {
            this.log.warn("Cannot find crystals, skipping deployment")
            return
        }
        if (this.router.list().length() != 0) {
            this.log.warn("Something already inside router")
            return
        }
        this.lastPlacedTime = currentTime
        this.log.info("Sending 4 crystal to router")
        let pushedItems = this.chest.pushItems(peripheral.getName(this.router), targetSlot, 4)
        if (pushedItems != 4) {
            this.log.warn("Issues with posting items! Don't know what happeend")
        }
    }

    protected configureInternal(): void {
        this.router = inventoryPeripheralProvider.findOrThrow("modularrouters:modular_router")
        this.chest = inventoryPeripheralProvider.findOrThrow("sophisticatedstorage:chest")
        this.subscribeToEvent(DRAGON_IS_NOT_HERE, (event: RedstoneEvent) =>
            this.moveCrystalas(event)
        );
    }

    protected perform(): void {
        this.log.info("Why do there is a perform again?")
    }
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new DragonController(
        os.getComputerLabel()
    )
);
agency.run();
