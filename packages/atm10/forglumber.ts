import * as doerlib from "cc-doerlib";
import * as logging from "cc-corelib/logging";
import * as turtle_ext from "cc-corelib/turtle_ext";
import * as bus from "cc-corelib/bus";
import { LumberReportEvent } from "./shared/forg_events";

const _log = logging.create("forglumber");

export class ForgLumberAgent extends doerlib.BaseAgent {
    normalVersion: string
    strippedVersion: string

    constructor(name: string, normalVersion: string = "productivetrees:soul_tree_log", strippedVersion: string = "productivetrees:soul_tree_stripped_log") {
        super(name, 5);
        this.normalVersion = normalVersion
        this.strippedVersion = strippedVersion
    }

    protected configureInternal(): void {}

    protected deployLog(): void {
        let logIndex = turtle_ext.searchItem(this.normalVersion);
        if (logIndex == -1) {
            _log.warn(
                "Cannot deploy log, it is not present in inventory"
            );
        } else {
            _log.info("Deploying log");
            turtle.select(logIndex);
            let result = turtle.placeUp();
            if (!result) {
                _log.error("Log deployment failed");
            }
        }
    }

    protected buildReport(): void {
        let logPresent = false;
        let slostsToClean: Array<number> = [];
        for (const slot of $range(1, 16)) {
            let item = turtle.getItemDetail(slot) as LuaTable;
            if (item != null) {
                if (item["name"] == this.normalVersion) {
                    logPresent = true;
                } else if (item["name"] == this.strippedVersion) {
                    slostsToClean = slostsToClean.concat([slot]);
                }
            }
        }
        bus.queueEvent(
            "forgoperator",
            new LumberReportEvent(this.name, !logPresent, slostsToClean)
        );
    }

    protected perform(): void {
        let [has_block, info] = turtle.inspectUp();
        if (has_block) {
            if (info["name"] != this.normalVersion) {
                if (info["name"] != this.strippedVersion) {
                    _log.error(
                        "Strange corrupted block here, it should not be the case"
                    );
                    return;
                }
                _log.info("Cleaning log before adding new one");
                let result = turtle.digUp();
                if (!result) {
                    _log.warn(
                        "For some reason cannot clear log, aborting this cycle"
                    );
                    return;
                }
                this.deployLog();
            } else {
                _log.info("Still just log");
            }
        } else {
            this.deployLog();
        }
        this.buildReport()
    }
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(new ForgLumberAgent(os.getComputerLabel()));
agency.run();
