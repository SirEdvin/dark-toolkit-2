import * as doerlib from "cc-doerlib";
import { RedstoneEvent } from "cc-corelib/event"
import { queueEvent } from "cc-corelib/bus"
import { DragonIsNotHereEvent } from "./shared/dragon_events";
import { universalSensorProvider, UniversalSensor } from 'typed-peripheral/pneumaticcraft/universal_sensor'

export class DragonNotifier extends doerlib.BaseAgent {

    sensor: UniversalSensor

    constructor(name: string) {
        super(name, 60);
    }

    protected dragonValidation(event?: any) {
        if (this.sensor.getSensorValue() == 0) {
            queueEvent("dragon_controller", new DragonIsNotHereEvent())
        }
    }

    protected configureInternal(): void {
        this.sensor = universalSensorProvider.findOrThrow()
        this.subscribeToEvent("redstone", (event: RedstoneEvent) =>
            this.dragonValidation(event)
        );
    }

    protected perform(): void {
        this.dragonValidation()
    }
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new DragonNotifier(
        os.getComputerLabel()
    )
);
agency.run();
