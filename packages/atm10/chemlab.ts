import * as doerlib from "cc-doerlib";
import { FluidCollectorAgent } from "cc-doerlib/extra/collectors";
import { FluidSpreaderAgent } from "cc-doerlib/extra/spreader";

let ae2Interface = peripheral.find("ae2:interface");

if (ae2Interface.length == 0) {
    throw "Cannot find chest with ore";
}

let ae2InterfaceName = peripheral.getName(ae2Interface[0]);

function regularOutputStrategy(name: string) {
    return ae2InterfaceName;
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(new FluidSpreaderAgent(
    os.getComputerLabel().concat("_sender"),
    1,
    ae2InterfaceName,
    (name) => name.startsWith("modern")
))
agency.addAgent(new FluidCollectorAgent(
    os.getComputerLabel().concat("_collector"),
    1,
    regularOutputStrategy,
    (name) => name.startsWith("modern"),
    false
))
agency.run();
