import * as doerlib from "cc-doerlib";
import { CollectorAgent, FluidCollectorAgent } from "cc-doerlib/extra/collectors";
import { COLLECTION_REPORT_EVENT } from "cc-doerlib/extra/events";
import { SpreaderAgent } from "cc-doerlib/extra/spreader";
import { RepublishingAgent } from "cc-doerlib/extra/republisher"
import { DictionarySyncAgent } from "cc-doerlib/extra/dictionary"

let ae2Interfaces = peripheral.find("ae2:interface");
let sophisticatedChests = peripheral.find("sophisticatedstorage:chest");

if (ae2Interfaces.length == 0) {
    throw "Cannot find ae2 interface";
}
if (sophisticatedChests.length == 0) {
    throw "Cannot find sophisticated chest";
}

let ae2InterfaceName = peripheral.getName(ae2Interfaces[0])
let sophistifacedChestName = peripheral.getName(sophisticatedChests[0])


function combSortingStrategy(name: string) {
    if (name == "productivebees:configurable_comb" || name.startsWith("productivebees:comb")) {
        return sophistifacedChestName;
    }
    return ae2InterfaceName
}

function regularOutputStrategy(name: string) {
    return ae2InterfaceName
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new CollectorAgent(
        os.getComputerLabel().concat("_hiver"),
        20,
        combSortingStrategy,
        (name: string) => name.startsWith("productivebees:expansion_box"),
        true,
        (name: string) => name != "productivebees:configurable_comb" && !name.startsWith("productivebees:comb")
    )
);
agency.addAgent(
    new SpreaderAgent(
        os.getComputerLabel().concat("_centrifiguer"),
        2,
        sophistifacedChestName,
        (name: string) => name.startsWith("productivebees:heated_centrifuge")
    )
);
agency.addAgent(
    new CollectorAgent(
        os.getComputerLabel().concat("_centrifuger_collector"),
        1,
        regularOutputStrategy,
        (name: string) => name.startsWith("productivebees:heated_centrifuge")
    )
);
agency.addAgent(
    new FluidCollectorAgent(
        os.getComputerLabel().concat("_centrifuger_fluid_collector"),
        0.5,
        regularOutputStrategy,
        (name: string) => name.startsWith("productivebees:heated_centrifuge"),
        false
    )
)
agency.addAgent(
    new RepublishingAgent(
        os.getComputerLabel(),
        [COLLECTION_REPORT_EVENT],
        "apiary_observer"
    )
)
agency.addAgent(
    new DictionarySyncAgent()
)
agency.run();
