import * as doerlib from "cc-doerlib";
import * as logging from "cc-corelib/logging";

const _log = logging.create("tpsmeter");

export class TPSMeterAgent extends doerlib.BaseAgent {
  constructor(name: string) {
    super(name, 10);
  }

  protected configureInternal(): void {}

  protected perform(): void {
    let redstoneLevel = redstone.getAnalogInput("right");
    let estimatedTPS = redstoneLevel * 1.33333;
    _log.info("Current tps", estimatedTPS.toFixed(2), "raw output", redstoneLevel)
  }
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(new TPSMeterAgent(os.getComputerLabel()));
agency.run();
