import * as doerlib from "cc-doerlib";
import { BaseAgent } from "cc-doerlib/agent"
import { CollectorAgent } from "cc-doerlib/extra/collectors";
import { COLLECTION_REPORT_EVENT, CollectionReportEvent } from "cc-doerlib/extra/events"
import { NAME_DICTIONARY } from "cc-corelib/extra/dictionaries"
import * as telem from "@siredvin/telem-ts"
import * as basalt from "@siredvin/basalt-ts"

let trashcans = peripheral.find("trashcans:item_trash_can_tile");

if (trashcans.length == 0) {
    throw "Cannot find trashcans";
}

let trashcanName = peripheral.getName(trashcans[0]);

function regularOutputStrategy(name: string) {
    return trashcanName;
}

peripheral.call('left', 'setTextScale', 0.5)

export class TranslutiveHistorizationAgent extends BaseAgent {
    data: Map<string, number>
    constructor(name: string) {
        super(name, 0)
        this.data = new Map()
    }

    public extractData(): Map<string, number> {
        const currentData = this.data
        this.data = new Map()
        currentData.forEach((value, key) => {
            this.data.set(key, 0)
        })
        return currentData
    }

    protected handleCollectionReport(event: CollectionReportEvent) {
        event.report.reports.forEach(report => {
            if (!this.data.has(report.name)) {
                this.data.set(report.name, report.amount)
            } else [
                this.data.set(report.name, this.data.get(report.name) + report.amount)
            ]
        })
    }

    protected configureInternal(): void {
        this.subscribeToEvent(COLLECTION_REPORT_EVENT, (event) => this.handleCollectionReport(event as CollectionReportEvent))
    }

    protected perform(): void {}
}

export class TelemAgent extends BaseAgent {
    backplane: telem.Backplane

    constructor(name: string, loopDelay: number, backplane: telem.Backplane) {
        super(name, loopDelay)
        this.backplane = backplane
    }
    protected configureInternal(): void {
    }
    protected perform(): void {
        this.backplane.cycle()
    }

}


let historyAgent = new TranslutiveHistorizationAgent(
    os.getComputerLabel()
)
const mon = basalt.addMonitor().setMonitor('left')
let backplane = telem.backplane().addInput("history", telem.input.custom(function() {
    const data = historyAgent.extractData()
    const newMetrics = {}
    data.forEach((value, key) => {
        newMetrics[NAME_DICTIONARY.get(key)] = value
    })
    return newMetrics
})).addOutput("chart", telem.output.basalt.graph(mon, "@history", colors.black, colors.red))
backplane.cache(true)

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new CollectorAgent(
        os.getComputerLabel().concat("_collector"),
        5,
        regularOutputStrategy,
        (name: string) => name.startsWith("minecraft:chest"),
    )
)
agency.addAgent(historyAgent)
agency.addAgent(
    new TelemAgent(os.getComputerLabel() + "_telem", 10, backplane)
)
agency.addCustomTask(function() { basalt.autoUpdate() }, function() { basalt.stopUpdate })
agency.run();
