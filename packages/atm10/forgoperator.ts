import * as doerlib from "cc-doerlib";
import { FluidCollectorAgent } from "cc-doerlib/extra/collectors";
import { LumberReportEvent, LUMBER_REPORT } from "./shared/forg_events";

import * as inventory_ext from "typed-peripheral/ext/inventory";
import { InventoryAPI, inventoryPeripheralProvider } from "typed-peripheral/apis/inventory";
import { turtleProvider } from "typed-peripheral/core/turtle";

let ae2Interfaces = peripheral.find("ae2:interface");
let trashcans = peripheral.find("trashcans:item_trash_can_tile");

if (ae2Interfaces.length == 0) {
    throw "Cannot find ae2 interface";
}
if (trashcans.length == 0) {
    throw "Cannot find trash can";
}

let ae2InterfaceName = peripheral.getName(ae2Interfaces[0]);
let trashcanName = peripheral.getName(trashcans[0]);

export class ForgOperatorAgent extends doerlib.BaseAgent {

    interfacePeripheral: InventoryAPI
    trashcanPeripheral: InventoryAPI
    labelNetworkMap: LuaTable<string, string>

    constructor(name: string) {
        super(name, 5);
    }

    protected handleLumberReport(event: LumberReportEvent) {
        if (event.needRestock) {
          let acaciaSlot = inventory_ext.searchItem(this.interfacePeripheral, "productivetrees:soul_tree_log")
          if (acaciaSlot == -1) {
            this.log.warn("Cannot find log in ae2 interface")
          } else {
            this.log.info("Restocking", event.lumberName)
            this.interfacePeripheral.pushItems(this.labelNetworkMap.get(event.lumberName), acaciaSlot)
          }
        }
        if (event.slotsToClean.length > 0) {
            this.log.info("Cleaning", event.lumberName)
            event.slotsToClean.forEach((slot) => {
                this.trashcanPeripheral.pullItems(this.labelNetworkMap.get(event.lumberName), slot)
            })
        }
    }

    protected discoverTurtles(): void {
        let turtles = turtleProvider.collect()
        this.labelNetworkMap = new LuaTable()
        turtles.forEach((trtl) => {
            this.labelNetworkMap.set(trtl.getLabel(), peripheral.getName(trtl))
        })
    }

    protected configureInternal(): void {
        this.subscribeToEvent(LUMBER_REPORT, (event: LumberReportEvent) =>
            this.handleLumberReport(event)
        );
        this.interfacePeripheral = inventoryPeripheralProvider.wrap(ae2InterfaceName)
        this.trashcanPeripheral = inventoryPeripheralProvider.wrap(trashcanName)
        this.discoverTurtles()
    }

    protected perform(): void {}
}

function regularOutputStrategy(name: string) {
    return ae2InterfaceName;
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new FluidCollectorAgent(
        os.getComputerLabel().concat("_fluid_collector"),
        40,
        regularOutputStrategy,
        (name: string) => name.startsWith("industrialforegoing:fluid")
    )
);
agency.addAgent(new ForgOperatorAgent(os.getComputerLabel()));
agency.run();
