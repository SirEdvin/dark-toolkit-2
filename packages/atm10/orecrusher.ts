import * as doerlib from "cc-doerlib";
import { CollectorAgent } from "cc-doerlib/extra/collectors";
import { SpreaderAgent } from "cc-doerlib/extra/spreader";
import { COLLECTION_REPORT_EVENT } from "cc-doerlib/extra/events";
import { RepublishingAgent } from "cc-doerlib/extra/republisher"
import { DictionarySyncAgent } from "cc-doerlib/extra/dictionary"

let chestWithOre = peripheral.find("sophisticatedstorage:chest");
let furnace = peripheral.find("ironfurnaces:unobtainium_furnace");

if (chestWithOre.length == 0) {
    throw "Cannot find chest with ore";
}
if (furnace.length == 0) {
    throw "Cannot find furnace";
}

let chestWithOreName = peripheral.getName(chestWithOre[0]);
let furnaceName = peripheral.getName(furnace[0]);

function regularOutputStrategy(name: string) {
    return furnaceName;
}

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new SpreaderAgent(
        os.getComputerLabel().concat("_ore_sender"),
        40,
        chestWithOreName,
        (name: string) => name.startsWith("ars_")
    )
)
agency.addAgent(
    new CollectorAgent(
        os.getComputerLabel().concat("_smelter"),
        5,
        regularOutputStrategy,
        (name: string) => name.startsWith("minecraft:barrel"),
    )
)
agency.addAgent(
    new RepublishingAgent(
        os.getComputerLabel(),
        [COLLECTION_REPORT_EVENT],
        "orecrusher_observer"
    )
)
agency.addAgent(
    new DictionarySyncAgent()
)
agency.run();
