import * as doerlib from "cc-doerlib";
import { ProductionHistorizationAgent } from "cc-doerlib/extra/historization"
import { DictionarySyncAgent } from "cc-doerlib/extra/dictionary"

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(
    new ProductionHistorizationAgent(
        os.getComputerLabel(),
    )
)
agency.addAgent(new DictionarySyncAgent())
agency.run();
