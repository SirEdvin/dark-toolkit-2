import * as logging from "cc-corelib/logging"

const _log = logging.create("test")

export class Step {
    constructor(readonly name: String, readonly func: () => void, readonly check: () => boolean) {}
}

export class Test {
    steps: Array<Step>
    constructor(readonly name: String) {this.steps = []}

    addStep(step: Step) {
        this.steps.push(step)
    }

    run(): Boolean {
        this.steps.forEach((step) => {
            _log.info("Starting step ", step.name, " for test ", this.name)
            try {
                step.func()
            } catch (anything) {
                _log.error("Exception ", anything, " when running step ", step.name, " for test ", this.name)
                return false
            }
            try {
                let isValid = step.check()
                if (!isValid) {
                    _log.error("Check for step ", step.name, " for test ", this.name, " is failed")
                    return false
                }
            } catch (anything) {
                _log.error("Exception ", anything, " when checking step ", step.name, " for test ", this.name)
                return false
            }
        })
        return true
    }
}