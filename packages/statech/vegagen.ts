import { hasSpaceForItem, searchItem } from "cc-corelib/turtle_ext"

while (true) {
    const [exists, info] = turtle.inspect()
    if (exists && info.get("name") == "spectrum:fading") {
        turtle.dig()    
        const slot = searchItem("minecraft:oak_leaves")
        if (slot != -1) {
            turtle.select(slot)
            turtle.place()
        }
    }
    os.sleep(0.2)
}