import * as doerlib from "cc-doerlib";
import { PerTargetLimitStrategyBuilder, SmartFluidTransferStep, SmartItemTransferStep, Transefer, anyPredicate, namePredicate, tagPredicate, targetIsEmpty } from "cc-doerlib/extra/transfer" 
import { PeripheralDiscovery, PeripheralDiscoveryByName, PeripheralDiscoveryByTypePattern } from "typed-peripheral/base"
import { fluidStoragePeripheralProvider, FluidStorageAPI } from "typed-peripheral/apis/fluid_storage"

let agency = new doerlib.Agency();
let waterTank = new PeripheralDiscoveryByName(fluidStoragePeripheralProvider, "create:fluid_tank_2")
let steamTank = new PeripheralDiscoveryByName(fluidStoragePeripheralProvider, "create:fluid_tank_1")
let steelBoilers = new PeripheralDiscoveryByTypePattern<FluidStorageAPI>("*steel_boiler*")
let chestWithCoal = new PeripheralDiscoveryByTypePattern("minecraft:chest")
agency.configure();
agency.addAgent(new Transefer(
    os.getComputerLabel(),
    2,
    [
        new SmartItemTransferStep(
            "spread_coal", chestWithCoal, steelBoilers, anyPredicate, new PerTargetLimitStrategyBuilder(8)
        ),
        new SmartFluidTransferStep(
            "spread_water", waterTank,  steelBoilers, anyPredicate
        ),
        new SmartFluidTransferStep(
            "collect_steam", steelBoilers, steamTank, namePredicate("modern_industrialization:steam")
        )
    ]
))
agency.run();
