import * as doerlib from "cc-doerlib";
import { Transefer, multiFilterPredicate, SmartItemTransferStep, GlobalLimitStrategyBuilder } from "cc-doerlib/extra/transfer" 
import { ItemDetail, PeripheralDiscoveryByTypePattern } from "typed-peripheral/base";
import { itemsToMacerate, tagsToMacerate } from "./shared/constants"

let agency = new doerlib.Agency();
agency.configure();

agency.addAgent(new Transefer(
    os.getComputerLabel(),
    1,
    [
        new SmartItemTransferStep(
            "extract", new PeripheralDiscoveryByTypePattern("*item_output*"), new PeripheralDiscoveryByTypePattern("ironchests:gold_chest"), (item: ItemDetail) => true
        ),
        new SmartItemTransferStep(
            "insert", new PeripheralDiscoveryByTypePattern("ironchests:gold_chest"), new PeripheralDiscoveryByTypePattern("*item_input*"), multiFilterPredicate(itemsToMacerate, tagsToMacerate),
            new GlobalLimitStrategyBuilder(64), 8
        )
    ]
))
agency.run();
