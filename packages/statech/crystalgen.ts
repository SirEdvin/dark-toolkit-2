import { hasSpaceForItem } from "cc-corelib/turtle_ext"

while (true) {
    const [existsUp, infoUp] = turtle.inspectUp()
    if (existsUp && infoUp.get("name") == "ae2:quartz_cluster") {
        if (hasSpaceForItem("ae2:certus_quartz_crystal", 4)) {
            turtle.digUp()
        }
    }
    const [exists, info] = turtle.inspect()
    if (exists && info.get("name") == "ae2:quartz_cluster") {
        if (hasSpaceForItem("ae2:certus_quartz_crystal", 4)) {
            turtle.dig()
        }
    }
    os.sleep(0.2)
}