import * as doerlib from "cc-doerlib";
import { Transefer, SmartItemTransferStep, namePredicate, multiFilterPredicate, negatePredicate, orPredicate } from "cc-doerlib/extra/transfer" 
import { itemsToMacerate, tagsToMacerate } from "./shared/constants"
import { PeripheralDiscoveryByTypePattern } from "typed-peripheral/base";

let agency = new doerlib.Agency();
agency.configure();

let maceratePredicate = multiFilterPredicate(itemsToMacerate, tagsToMacerate)
let coalPredicate = namePredicate("modern_industrialization:coal_dust")

agency.addAgent(new Transefer(
    os.getComputerLabel(),
    5,
    [
        new SmartItemTransferStep(
            "send_everything_to_storage", new PeripheralDiscoveryByTypePattern("ironchests:gold_chest"), new PeripheralDiscoveryByTypePattern("toms_storage:ts.inventory_cable_connector"), negatePredicate(maceratePredicate)
        )
    ]
))
agency.run();
