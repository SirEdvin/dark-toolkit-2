import * as doerlib from "cc-doerlib";
import { SmartItemTransferStep, Transefer, tagPredicate, targetIsEmpty } from "cc-doerlib/extra/transfer" 

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(new Transefer(
    os.getComputerLabel(),
    5,
    [
        new SmartItemTransferStep(
            "extract_plates", "create:depot", "toms_storage:ts.inventory_cable_connector", tagPredicate("c:plates")
        ),
        new SmartItemTransferStep(
            "place_ingots", "minecraft:chest", "create:depot", tagPredicate("c:ingots"), 64, 1, targetIsEmpty
        ),
    ]
))
agency.run();
