import * as doerlib from "cc-doerlib";
import { Transefer, multiFilterPredicate, SmartItemTransferStep, targetIsEmpty, GlobalLimitStrategy, GlobalLimitStrategyBuilder } from "cc-doerlib/extra/transfer" 
import { PeripheralDiscoveryByName, PeripheralDiscoveryByTypePattern } from "typed-peripheral/base";

const itemsToSmelt = {
    "minecraft:clay_ball": true,
    "minecraft:cobblestone": true,
    "minecraft:sand": true,
    "minecraft:basalt": true,
    "minecraft:smooth_basalt": true
}

const itemsToExtract = {
    "modern_industrialization:fire_clay_brick": true,
    "minecraft:stone": true,
    "minecraft:glass": true,
    "spectrum:polished_basalt": true,
    "ae2:silicon": true
}

const tagsToSmelt = [
    "c:dusts"
]

const tagsToExtract = [
    "c:ingots"
]

let agency = new doerlib.Agency();
agency.configure();
let createDepot = new PeripheralDiscoveryByTypePattern("create:depot")
let inventory = new PeripheralDiscoveryByTypePattern("toms_storage:ts.inventory_cable_connector")
let chest = new PeripheralDiscoveryByTypePattern("minecraft:chest")

agency.addAgent(new Transefer(
    os.getComputerLabel(),
    5,
    [
        new SmartItemTransferStep(
            "extract", createDepot, inventory, multiFilterPredicate(itemsToExtract, tagsToExtract)
        ),
        new SmartItemTransferStep(
            "put_inside", chest, createDepot, multiFilterPredicate(itemsToSmelt, tagsToSmelt), new GlobalLimitStrategyBuilder(64), 1, targetIsEmpty
        )
    ]
))
agency.run();
