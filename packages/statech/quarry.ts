import * as doerlib from "cc-doerlib";
import { SmartItemTransferStep, Transefer, anyPredicate, tagPredicate } from "cc-doerlib/extra/transfer" 
import { MetricCollectionAgent } from "cc-doerlib/extra/metrics"
import { PeripheralDiscoveryByTypePattern } from "typed-peripheral/base";

let agency = new doerlib.Agency();
agency.configure();
agency.addAgent(new Transefer(
    os.getComputerLabel() + "_transfer",
    3,
    [
        new SmartItemTransferStep(
            "extract_ore", new PeripheralDiscoveryByTypePattern("*item_output*"), new PeripheralDiscoveryByTypePattern("ironchests:gold_chest"), anyPredicate
        ),
    ]
))
agency.addAgent(
    new MetricCollectionAgent(
        os.getComputerLabel(), 5, "quarry"
    )
)
agency.run();
