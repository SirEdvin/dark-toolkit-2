export const itemsToMacerate = {
    "minecraft:brick": true,
    "minecraft:clay_ball": true,
    "modern_industrialization:coke": true,
    "modern_industrialization:coal_crushed_dust": true,
    "modern_industrialization:redstone_crushed_dust": true,
    "modern_industrialization:lignite_coal_crushed_dust": true,
    "modern_industrialization:bauxite_crushed_dust": true,
    "modern_industrialization:lapis_crushed_dust": true
}

export const tagsToMacerate = [
    "c:ores",
    "c:raw_ores",
    "c:raw_materials"
]