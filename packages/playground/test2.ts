import * as telem from "@siredvin/telem-ts"
import * as basalt from "@siredvin/basalt-ts"

const mon = basalt.addMonitor().setMonitor('right')

const backplane = telem.backplane().addInput(
    "custom_rand", telem.input.custom(function() {
        return {
            rand: math.random() * 10000000
        }
    })
).addOutput("monitor_rand1", telem.output.basalt.graph(mon, "rand", colors.black, colors.red))
parallel.waitForAny(
    backplane.cycleEvery(0.1),
    basalt.autoUpdate
)