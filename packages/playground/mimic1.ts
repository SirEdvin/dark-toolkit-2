import { mimicProvider } from "typed-peripheral/turtlematic/mics/mimic"

const mimic = mimicProvider.findOrThrow()
const stepAmount = 80
const halfStepAmount = stepAmount / 2
const maxScale = 1
const minScale = 0.2
const scaleShift = maxScale - minScale

const initialPhaseShift = parseInt(settings.get("dark_toolkit.mimic_phase_shift", "0"))

function buildRLM(tick: number): string {
    const step = tick % stepAmount
    let normalizedStep = step > halfStepAmount ? stepAmount - step : step
    const scale = minScale + normalizedStep * scaleShift / halfStepAmount
    const scaleCommand = "s(" + scale + "," + scale + "," + scale + ")"
    const lostSize = (maxScale - scale) / 2
    const transformCommand = "t(" + lostSize + "," + lostSize + "," + lostSize + ")"
    return transformCommand + ";" + scaleCommand
}

let luaTable = new LuaTable()
luaTable["block"] = "minecraft:oak_log"
mimic.setMimic(luaTable)
while (true) {
    let rlm = buildRLM((os.epoch("utc") / 50) + initialPhaseShift)
    mimic.setTransformation(rlm)
    os.sleep(0.05)
}