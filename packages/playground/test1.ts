import { statsDBridgeProvider } from "typed-peripheral/datafortress/statsdBridge"

const statsd_bridge = statsDBridgeProvider.findOrThrow()

while (true) {
    statsd_bridge.delta("test1", 1)
    print("send another increment")
    os.sleep(0.1)
}