import { queueEvent, broadcastEvent } from 'cc-corelib/bus'
import { NAME_DICTIONARY } from "cc-corelib/extra/dictionaries"
import { BaseAgent } from '../agent'
import { DICTIONARY_POPULATING_EVENT, DICTIONARY_REQUEST_EVENT, DictionaryPopulatingEvent, DictionaryRequestEvent } from './events'

export function requestDictionaryKey(key: string) {
    broadcastEvent(new DictionaryRequestEvent(key, os.getComputerLabel()))
}

export class DictionarySyncAgent extends BaseAgent {

    constructor() {
        super(os.getComputerLabel().concat("_dictionary"), -1)
    }

    protected configureInternal(): void {
        this.subscribeToEvent(DICTIONARY_POPULATING_EVENT, (event: DictionaryPopulatingEvent) => {
            if (!NAME_DICTIONARY.has(event.key)) {
                NAME_DICTIONARY.set(event.key, event.value)
            }
        })
        this.subscribeToEvent(DICTIONARY_REQUEST_EVENT, (event: DictionaryRequestEvent) => {
            if (NAME_DICTIONARY.has(event.key)) {
                queueEvent(event.target, new DictionaryPopulatingEvent(event.key, NAME_DICTIONARY.get(event.key)))
            }
        })
    }
    protected perform(): void {
        throw new Error('Method not implemented.')
    }
    
}