import { BaseAgent } from '../agent'
import { NAME_DICTIONARY, PersistentDictionary } from "cc-corelib/extra/dictionaries"
import { TRANSFER_EVENT, TransferEvent } from "./events"
import * as telem from "@siredvin/telem-ts"

export class MetricCollectionAgent extends BaseAgent {
    transferCounters: PersistentDictionary<string, number>
    backplane: telem.Backplane

    constructor(name: string, loopDelay: number, readonly adapterName: string, dbName: string = "transfer.db") {
        super(name, loopDelay)
        this.transferCounters = new PersistentDictionary(dbName)
    }

    private handleTransferEvent(event: TransferEvent) {
        this.transferCounters.set(event.record.item, event.record.count + this.transferCounters.get(event.record.item, 0))
    }

    protected getMetrics(): LuaMultiReturn<telem.Metric[]> {
        const metrics = []
        for (let [item, count] of this.transferCounters.table) {
            metrics.push(
                telem.metric({
                    name: NAME_DICTIONARY.get(item, item).toLowerCase().replace(" ", "_"),
                    value: count,
                    unit: "si:items",
                    source: this.name
                })
            )
        }
        return $multi(...metrics)
    }

    protected getHealthcheck(): LuaMultiReturn<telem.Metric[]> {
        return $multi(telem.metric("healthcheck", os.time()))
    }

    protected configureInternal(): void {
        this.subscribeToEvent(TRANSFER_EVENT, (event: TransferEvent) => this.handleTransferEvent(event))
        const authGrafana = {
            endpoint: settings.get("dark_toolkit.grafana_endpoint", ""),
            apiKey: settings.get("dark_toolkit.grafana_api_key", ""),
        }
        this.backplane = telem.backplane().addInput(
            this.adapterName, telem.input.custom(() => this.getMetrics())
        ).addInput(
            this.adapterName + "_healthcheck", telem.input.custom(() => this.getHealthcheck())
        ).addOutput(
            "grafana", telem.output.grafana(authGrafana.endpoint, authGrafana.apiKey)
        )
    }
    protected perform(): void {
        this.backplane.cycle()
    }
    
}