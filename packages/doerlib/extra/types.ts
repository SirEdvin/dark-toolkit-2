export type TargetSelectionStrategy = (name: string) => string | null;

export class SingleCollectionReport {
    constructor(readonly name: string, readonly amount: number) {}
}

export class CollectionReport {
    constructor(readonly reports: Array<SingleCollectionReport>) {}
}


export class TransferRecord {
    constructor(
        readonly sourceName: string,
        readonly targetName: string,
        readonly item: string,
        readonly count: number
    ) {}
}