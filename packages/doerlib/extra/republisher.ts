import { queueEvent } from 'cc-corelib/bus'
import { BaseAgent } from '../agent'

export class RepublishingAgent extends BaseAgent {
    republishingEvents: Array<string>
    target: string

    constructor(name: string, republishingEvents: Array<string>, target: string) {
        super(name, null)
        this.republishingEvents = republishingEvents
        this.target = target
    }

    protected configureInternal(): void {
        this.republishingEvents.forEach((name) => {
            this.subscribeToEvent(name, event => queueEvent(this.target, event))
        })
    }
    protected perform(): void {}

}