import { Timeseries, TimeseriesRecord } from "cc-corelib/structures/timeseries"
import { BaseAgent } from '../agent'
import { COLLECTION_REPORT_EVENT, CollectionReportEvent } from "./events"
import { CollectionReport } from "./types"
import { NAME_DICTIONARY } from "cc-corelib/extra/dictionaries"
import { requestDictionaryKey } from "./dictionary"

export abstract class AbstractHistorizationAgent extends BaseAgent {
    ts: Timeseries<CollectionReport>
    monitors: LuaMultiReturn<MonitorPeripheral[]>
    rate: Map<string, number>

    constructor(name: string, limit: number = 500) {
        super(name, 0)
        let loadedTS = this.load()
        if (loadedTS != null) {
            this.ts = loadedTS
        } else {
            this.ts = new Timeseries(limit)
        }
        this.rate = new Map()
    }

    protected get fileName(): string {
        return `${this.name}.db`
    }

    protected configureInternal(): void {
        this.subscribeToEvent(COLLECTION_REPORT_EVENT, (event) => this.handleCollectionReport(event as CollectionReportEvent))
        this.monitors = peripheral.find("monitor") as LuaMultiReturn<MonitorPeripheral[]>
        if (this.monitors == null || this.monitors.length == 0) {
            throw "Cannot find any monitor"
        }
    }

    protected handleCollectionReport(event: CollectionReportEvent) {
        this.ts.append(new TimeseriesRecord(event.report))
        this.save()
        this.update()
    }
    protected save() {
        let f = fs.open(this.fileName, "w")
        if (f[0] == null) {
            this.log.error("Cannot open file to save data")
        } else {
            let handler = f[0] as FileHandle
            handler.write(textutils.serialise(this.ts))
            handler.close()
        }
    }

    protected load(): Timeseries<CollectionReport> | null {
        if (fs.exists(this.fileName)) {
            let f = fs.open(this.fileName, "r")
            if (f[0] == null) {
                this.log.error("Cannot open file to load data")
            } else {
                let handler = f[0] as FileHandle
                let data = handler.readAll()
                let loadedTS = textutils.unserialise(data)
                if (loadedTS != null) {
                    let ts = new Timeseries<CollectionReport>(loadedTS.limit)
                    ts.values = loadedTS.values
                    handler.close()
                    return ts
                }
                handler.close()
            }
        }
        return null
    }

    protected update() {
        this.recalculate()
        this.redraw()
    }

    protected recalculate() {
        let timeframe = this.ts.timeframe / 1000
        let counter = new Map<string, number>();
        this.ts.values.forEach((value) => {
            value.value.reports.forEach((report) => {
                if (!counter.has(report.name)) {
                    counter.set(report.name, report.amount)
                } else {
                    counter.set(report.name, counter.get(report.name) + report.amount)
                }
            })
        })
        this.rate = new Map(Array.from(counter.entries()).map(value => {
            return [value[0], 60 * value[1] / timeframe]
        }))
        this.log.debug("Calculating new rate", this.rate.size, "records build")
    }

    protected getDisplayName(name: string): string {
        if (NAME_DICTIONARY.has(name)) {
            return NAME_DICTIONARY.get(name)
        }
        requestDictionaryKey(name)
        return name
    }

    protected redraw() {
        let monitorIndex = 0
        this.monitors.forEach(m => {
            m.clear()
            m.setCursorPos(1, 1)
        })
        let currentMonitor = this.monitors[monitorIndex]
        let [currentMonitorWidth, currentMonitorHeight] = currentMonitor.getSize()
        let usedMonitorWidth = 1
        let usedMonitorHeight = 1
        let exchaused = false
        Array.from(this.rate.keys()).sort((a, b) => this.sortNames(a, b)).forEach(name => {
            if (!exchaused) {
                let valueRate = this.rate.get(name)
                if (valueRate != 0) {
                    currentMonitor.write(`${this.getDisplayName(name)}: ${valueRate.toFixed(2)} i/m`)
                    // Updating monitor selection
                    usedMonitorHeight += 1
                    if (usedMonitorHeight >= currentMonitorHeight) {
                        usedMonitorWidth += 39
                        usedMonitorHeight = 1
                        if (usedMonitorWidth + 39 >= currentMonitorWidth) {
                            monitorIndex += 1
                            if (monitorIndex >= this.monitors.length) {
                                exchaused = true
                            } else {
                                currentMonitor = this.monitors[monitorIndex] as MonitorPeripheral
                                [currentMonitorWidth, currentMonitorHeight] = currentMonitor.getSize()
                                usedMonitorWidth = 1
                                usedMonitorHeight = 1
                            }
                        } else {
                            currentMonitor.setCursorPos(usedMonitorWidth, usedMonitorHeight)    
                        }
                    } else {
                        currentMonitor.setCursorPos(usedMonitorWidth, usedMonitorHeight)
                    }
                }
            }
        })
    }

    protected perform(): void {}

    protected abstract sortNames(a: string, b: string): number
}

export class HistorizationAgent extends AbstractHistorizationAgent {
    protected sortNames(a: string, b: string): number {
        if (a == b) return 0
        return this.getDisplayName(a) > this.getDisplayName(b) ? 1: -1
    }
    
}

export class ProductionHistorizationAgent extends AbstractHistorizationAgent {
    protected sortNames(a: string, b: string): number {
        if (a == b) return 0
        let aRate = this.rate.get(a) || 0
        let bRate = this.rate.get(b) || 0
        return aRate < bRate ? 1: -1
    }   
}