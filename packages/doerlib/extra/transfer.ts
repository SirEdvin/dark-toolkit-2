import * as logging from "cc-corelib/logging"

import { BaseAgent } from '../agent'
import {
    InventoryAPI,
} from "typed-peripheral/apis/inventory";
import {
    ItemStorageAPI,
} from "typed-peripheral/apis/item_storage";
import {
    CombinedPredicate,
    FluidDetail,
    FluidPredicate,
    PeripheralDiscovery,
    PeripheralPredicate,
} from "typed-peripheral/base";
import { ItemDetail, ItemPredicate } from "typed-peripheral/base"
import { FluidStorageAPI } from "typed-peripheral/apis/fluid_storage";
import { NAME_DICTIONARY } from "cc-corelib/extra/dictionaries"
import { TransferRecord } from "./types"
import { TransferEvent } from "./events"
import { queueLocalEvent } from "cc-corelib/bus"

const _log = logging.create("transfer")

export abstract class TransferStep {
    // TODO: add info about transfered items later
    abstract transfer()
}

export function anyPredicate(item: ItemDetail | FluidDetail): boolean {
    return true
}

export function multiFilterPredicate(itemFilter: { [key: string]: boolean }, tagFilter: string[]): ItemPredicate {
    return function(item: ItemDetail) {
        if (item == null) return false
        if (itemFilter[item.name])
            return true
        if (tagFilter.find(value => item.tags[value]) != undefined)
            return true
        return false
    }
}

export function namePredicate(name: string): CombinedPredicate {
    return function(item: ItemDetail | FluidDetail) {
        if (item == null) return false
        return item.name == name
    }
}

export function tagPredicate(tag: string): ItemPredicate {
    return function(item: ItemDetail) {
        if (item == null) return false
        return item.tags[tag]
    }
}

export function andPredicate(first: ItemPredicate, second: ItemPredicate): ItemPredicate {
    return function(item: ItemDetail) {
        if (item == null) return false
        return first(item) && second(item)
    }
}


export function orPredicate(first: ItemPredicate, second: ItemPredicate): ItemPredicate {
    return function(item: ItemDetail) {
        return first(item) || second(item)
    }
}

export function negatePredicate(predicate: ItemPredicate) {
    return function (item: ItemDetail) {
        return !predicate(item)
    }
}

export function targetIsEmpty(p: IPeripheral): boolean {
    if (peripheral.hasType(p, "item_storage")) {
        let items = (p as ItemStorageAPI).items()
        return items != null && items.isEmpty()
    }
    if (peripheral.hasType(p, "inventory")) {
        let inv = (p as InventoryAPI).list()
        return inv != null && inv.isEmpty()
    }
    return false
}

export abstract class LimitStrategy {
    trackedTransfers: TransferRecord[]

    constructor(
        readonly sources: IPeripheral[],
        readonly targets: IPeripheral[]
    ) {
        this.trackedTransfers = []
    }

    trackTransfer(source: IPeripheral, target: IPeripheral, item: string, count: number) {
        this.trackedTransfers.push(new TransferRecord(
            peripheral.getName(source),
            peripheral.getName(target),
            item,
            count
        ))
    }
    abstract getLimit(source: IPeripheral, target: IPeripheral, availableAmount: number)
    abstract isTransferFinished(): boolean
    abstract getTransferedAmount(): number
}

export class GlobalLimitStrategy extends LimitStrategy {
    transferedAmount: number
    
    constructor(
        sources: IPeripheral[],
        targets: IPeripheral[],
        readonly limit: number
    ) {
        super(sources, targets)
        this.transferedAmount = 0
    }

    track

    trackTransfer(source: IPeripheral, target: IPeripheral, item: string, count: number) {
        super.trackTransfer(source, target, item, count)
        if (count != null)
            this.transferedAmount += count
    }
    getLimit(source: IPeripheral, target: IPeripheral, availableAmount: number) {
        const availableLimit = this.limit - this.transferedAmount
        if (availableAmount <= 0)
            return 0
        return math.min(availableAmount, availableLimit)
    }
    isTransferFinished(): boolean {
        return this.transferedAmount >= this.limit
    }
    getTransferedAmount(): number {
        return this.transferedAmount
    }
}

export class PerTargetLimitStrategy extends LimitStrategy {
    transferedAmount: LuaTable<string, number>
    
    constructor(
        sources: IPeripheral[],
        targets: IPeripheral[],
        readonly limit: number
    ) {
        super(sources, targets)
        this.transferedAmount = new LuaTable()
        this.targets.forEach(t => this.transferedAmount.set(peripheral.getName(t), 0))
    }

    trackTransfer(source: IPeripheral, target: IPeripheral, item: string, count: number) {
        super.trackTransfer(source, target, item, count)
        const targetName = peripheral.getName(target)
        this.transferedAmount.set(targetName, this.transferedAmount.get(targetName) + count)
    }

    getLimit(source: IPeripheral, target: IPeripheral, availableAmount: number) {
        const targetName = peripheral.getName(target)
        const availableLimit = this.limit - this.transferedAmount.get(targetName)
        return math.min(availableAmount, availableLimit)
    }

    getTransferedAmount(): number {
        let sum = 0
        for (let [name, amount] of this.transferedAmount) {
            sum += amount
        }
        return sum
    }

    isTransferFinished(): boolean {
        for (let [name, amount] of this.transferedAmount) {
            if (amount < this.limit)
                return false
        }
        return true
    }
    
}

export abstract class LimitStrategyBuilder{
    abstract build(sources: IPeripheral[], targets: IPeripheral[]): LimitStrategy
}

export class GlobalLimitStrategyBuilder extends LimitStrategyBuilder{
    constructor(
        readonly limit: number
    ) {
        super()
    }

    build(sources: IPeripheral[], targets: IPeripheral[]) {
        return new GlobalLimitStrategy(sources, targets, this.limit)
    }
}

export class PerTargetLimitStrategyBuilder extends LimitStrategyBuilder{
    constructor(
        readonly limit: number
    ) {
        super()
    }

    build(sources: IPeripheral[], targets: IPeripheral[]) {
        return new PerTargetLimitStrategy(sources, targets, this.limit)
    }

}

export class SmartItemTransferStep extends TransferStep {
    private sourcePeripherals: IPeripheral[]
    private targetPeripherals: IPeripheral[]
    private counter: number
    constructor(
        readonly name: string,
        readonly sourceDiscovery: PeripheralDiscovery<IPeripheral>,
        readonly targetDiscovery: PeripheralDiscovery<IPeripheral>,
        readonly materialPredicate: ItemPredicate | CombinedPredicate,
        readonly limit: LimitStrategyBuilder = new GlobalLimitStrategyBuilder(2048),
        readonly batchSize: number = 1,
        readonly targetPredicate: PeripheralPredicate = (info: IPeripheral) => true,
        readonly refreshPeriod: number = 128
    ) {
        super()
        this.refreshPeripherals()
        this.counter = 0
    }

    private refreshPeripherals() {
        this.sourcePeripherals = this.sourceDiscovery.discover()
        this.targetPeripherals = this.targetDiscovery.discover()
        _log.info("Bootstrap transfer step", this.name, "with", this.sourcePeripherals.length, "sources and", this.targetPeripherals.length, "targets")
    }

    private items(somePeripheral: IPeripheral): LuaTable<number, ItemDetail> {
        if (peripheral.hasType(somePeripheral, "item_storage")) {
            return (somePeripheral as ItemStorageAPI).items()
        } else if (peripheral.hasType(somePeripheral, "inventory")) {
            const inventoryPeripheral = somePeripheral as InventoryAPI
            let invList = inventoryPeripheral.list()
            if (invList != null) {
                const newTable = new LuaTable<number, ItemDetail>()
                for (let [slot, item] of invList) {
                    const itemDetail = inventoryPeripheral.getItemDetail(slot)
                    if (itemDetail != null) {
                        newTable.set(slot, itemDetail)
                    }
                }
                return newTable
            }
        }
        throw "Cannot search items for peripheral like this: " + peripheral.getName(somePeripheral)
    }

    private moveItems(sourcePeripheral: IPeripheral, targetPeripheral: IPeripheral, slot: number, item: ItemDetail, amountToTransfer: number): number {
        if (peripheral.hasType(sourcePeripheral, "item_storage"))
            return (sourcePeripheral as ItemStorageAPI).pushItem(peripheral.getName(targetPeripheral), item.name, amountToTransfer)
        if (peripheral.hasType(targetPeripheral, "item_storage"))
            return (targetPeripheral as ItemStorageAPI).pullItem(peripheral.getName(sourcePeripheral), item.name, amountToTransfer)
        return (sourcePeripheral as InventoryAPI).pushItems(peripheral.getName(targetPeripheral), slot, amountToTransfer)
    }

    private singleTransfer(sourcePeripheral: IPeripheral, limit: LimitStrategy) {
        let items = this.items(sourcePeripheral)
        if (items == null) {
            _log.info("Source peripheral", peripheral.getName(sourcePeripheral), "is empty")
            return 0
        }
        for (let [slot, item] of items) {
            if (this.materialPredicate(item) && item.count != null && item.count >= this.batchSize) {
                for (let targetPeripheral of this.targetPeripherals) {
                    if (this.targetPredicate(targetPeripheral)) {
                        let amountToTransfer = limit.getLimit(sourcePeripheral, targetPeripheral, item.count)
                        amountToTransfer = amountToTransfer - (amountToTransfer % this.batchSize)
                        const thisTransferedAmount = this.moveItems(sourcePeripheral, targetPeripheral, slot, item, amountToTransfer)
                        if (thisTransferedAmount != null && thisTransferedAmount != 0) {
                            _log.debug("Transfered", item.name, thisTransferedAmount, "from", peripheral.getName(sourcePeripheral), "to", peripheral.getName(targetPeripheral))
                            limit.trackTransfer(sourcePeripheral, targetPeripheral, item.name, thisTransferedAmount)
                            NAME_DICTIONARY.track(item.name, item.displayName)
                            if (limit.isTransferFinished()) {
                                _log.debug("Transfer consider ended")
                                return
                            }
                        } else {
                            _log.debug("It seems that", peripheral.getName(targetPeripheral), "doesn't want to access items")
                        }
                    }
                }
            }
        }        
    }

    transfer() {
        const limitStrategy = this.limit.build(this.sourcePeripherals, this.targetPeripherals)
        for (let peripheral of this.sourcePeripherals) {
            this.singleTransfer(peripheral, limitStrategy)
            if (limitStrategy.isTransferFinished()) {
                _log.debug("Transfer consider ended")
                break
            }
        }
        _log.debug("Finished transfer step", this.name, "transfered", limitStrategy.getTransferedAmount(), "items")
        limitStrategy.trackedTransfers.forEach(t => {
            queueLocalEvent(new TransferEvent(this.name, t))
        })
        this.counter += 1
        if (this.counter >= this.refreshPeriod) {
            this.refreshPeripherals()
            this.counter = 0
        }
    }
}

export class SmartFluidTransferStep extends TransferStep {
    private sourcePeripherals: FluidStorageAPI[]
    private targetPeripherals: FluidStorageAPI[]
    private counter: number
    constructor(
        readonly name: string,
        readonly sourceDiscovery: PeripheralDiscovery<FluidStorageAPI>,
        readonly targetDiscovery: PeripheralDiscovery<FluidStorageAPI>,
        readonly materialPredicate: FluidPredicate | CombinedPredicate,
        readonly limit: LimitStrategyBuilder = new GlobalLimitStrategyBuilder(1024000),
        readonly batchSize: number = 1,
        readonly targetPredicate: PeripheralPredicate = (info: IPeripheral) => true,
        readonly refreshPeriod: number = 128
    ) {
        super()
        this.refreshPeripherals()
        this.counter = 0
    }

    private refreshPeripherals() {
        this.sourcePeripherals = this.sourceDiscovery.discover()
        this.targetPeripherals = this.targetDiscovery.discover()
        _log.info("Bootstrap transfer step", this.name, "with", this.sourcePeripherals.length, "sources and", this.targetPeripherals.length, "targets")
    }

    private singleTransfer(sourcePeripheral: FluidStorageAPI, limit: LimitStrategy) {
        let tanks = sourcePeripheral.tanks()
        if (tanks == null) {
            _log.info("Source peripheral", peripheral.getName(sourcePeripheral), "is empty")
            return 0
        }
        for (let [slot, fluid] of tanks) {
            if (this.materialPredicate(fluid) && fluid.amount != null && fluid.amount >= this.batchSize) {
                for (let targetPeripheral of this.targetPeripherals) {
                    if (this.targetPredicate(targetPeripheral)) {
                        let amountToTransfer = limit.getLimit(sourcePeripheral, targetPeripheral, fluid.amount)
                        amountToTransfer = amountToTransfer - (amountToTransfer % this.batchSize)
                        const thisTransferedAmount = sourcePeripheral.pushFluid(peripheral.getName(targetPeripheral), amountToTransfer, fluid.name)
                        if (thisTransferedAmount != null && thisTransferedAmount != 0) {
                            limit.trackTransfer(sourcePeripheral, targetPeripheral, fluid.name, thisTransferedAmount)
                            if (limit.isTransferFinished())
                                return
                        } else {
                            _log.info("It seems that", peripheral.getName(targetPeripheral), "doesn't want to access fluid")
                        }
                    }
                }
            }
        }        
    }

    transfer() {
        const limitStrategy = this.limit.build(this.sourcePeripherals, this.targetPeripherals)
        for (let peripheral of this.sourcePeripherals) {
            this.singleTransfer(peripheral, limitStrategy)
            if (limitStrategy.isTransferFinished())
                break
        }
        _log.info("Finished transfer step", this.name, "transfered", limitStrategy.getTransferedAmount(), "mb")
        limitStrategy.trackedTransfers.forEach(t => {
            queueLocalEvent(new TransferEvent(this.name, t))
        })
        this.counter += 1
        if (this.counter >= this.refreshPeriod) {
            this.refreshPeripherals()
            this.counter = 0
        }
    }
}


export class Transefer extends BaseAgent {

    constructor(
        name: string,
        loopDelay: number,
        readonly steps: Array<TransferStep>
    ) {
        super(name, loopDelay)
    }

    protected configureInternal(): void {
    }

    protected perform(): void {
        this.steps.forEach(step => step.transfer())
    }
}
