import * as events from "cc-corelib/event"
import { CollectionReport, TransferRecord } from "./types"

export const COLLECTION_REPORT_EVENT = "collection_report"
export const DICTIONARY_REQUEST_EVENT = "dictonary_request"
export const DICTIONARY_POPULATING_EVENT = "dictionary_populating"
export const TRANSFER_EVENT = "transfer"

export class CollectionReportEvent implements events.IEvent {

    constructor(readonly collectorName: string, readonly report: CollectionReport) {}

    get_name(): string {
        return COLLECTION_REPORT_EVENT
    }
    get_args(): any[] {
        return [this.collectorName, this.report]
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != COLLECTION_REPORT_EVENT) return null;
        let ev = new CollectionReportEvent(args[1], args[2]);
        return ev;
    }
}

export class DictionaryRequestEvent implements events.IEvent {
    constructor(readonly key: string, readonly target: string) {}

    get_name(): string {
        return DICTIONARY_REQUEST_EVENT
    }
    get_args(): any[] {
        return [this.key, this.target]
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != DICTIONARY_REQUEST_EVENT) return null;
        let ev = new DictionaryRequestEvent(args[1], args[2]);
        return ev;
    }

}

export class DictionaryPopulatingEvent implements events.IEvent {
    constructor(readonly key: string, readonly value: string) {}

    get_name(): string {
        return DICTIONARY_POPULATING_EVENT
    }
    get_args(): any[] {
        return [this.key, this.value]
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != DICTIONARY_POPULATING_EVENT) return null;
        let ev = new DictionaryPopulatingEvent(args[1], args[2]);
        return ev;
    }

}

export class TransferEvent implements events.IEvent {
    constructor(readonly agent: string, readonly record: TransferRecord) {}

    get_name(): string {
        return TRANSFER_EVENT
    }
    get_args(): any[] {
        return [this.agent, this.record]
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != TRANSFER_EVENT) return null;
        let ev = new TransferEvent(args[1], args[2]);
        return ev;
    }
}

events.eventInitializers.push(CollectionReportEvent.init)
events.eventInitializers.push(DictionaryRequestEvent.init)
events.eventInitializers.push(DictionaryPopulatingEvent.init)
events.eventInitializers.push(TransferEvent.init)
