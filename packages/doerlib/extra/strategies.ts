import { IPeripheralProvider } from "typed-peripheral/base";
import {
    InventoryAPI,
    inventoryPeripheralProvider,
} from "typed-peripheral/apis/inventory";
import {
    FluidStorageAPI,
    fluidStoragePeripheralProvider,
} from "typed-peripheral/apis/fluid_storage";
import { NAME_DICTIONARY } from "cc-corelib/extra/dictionaries";

export abstract class OperatorStrategy<T> {
    abstract get peripheralProvider(): IPeripheralProvider<T>
    abstract push(peripheral: T, target: string, slot: number, name: string): number | null
    abstract list(peripheral: T): LuaTable<AnyNotNil, any>
    newNameHandler(peripheral: T, name: string, slot: number) {
    }
}


export class InventoryStrategy extends OperatorStrategy<InventoryAPI> {
    get peripheralProvider(): IPeripheralProvider<InventoryAPI> {
        return inventoryPeripheralProvider
    }
    push(peripheral: InventoryAPI, target: string, slot: number, name: string): number | null {
        return peripheral.pushItems(target, slot)
    }
    list(peripheral: InventoryAPI): LuaTable<AnyNotNil, any> {
        return peripheral.list()
    }

    newNameHandler(peripheral: InventoryAPI, name: string, slot: number): void {
        NAME_DICTIONARY.set(name, peripheral.getItemDetail(slot).displayName)
    }
}

export class FluidStorageStrategy extends OperatorStrategy<FluidStorageAPI> {
    fluidTransferLimit: number;

    constructor(fluidTransferLimit: number = 250000) {
        super()
        this.fluidTransferLimit = fluidTransferLimit
    }

    get peripheralProvider(): IPeripheralProvider<FluidStorageAPI> {
        return fluidStoragePeripheralProvider
    }

    push(peripheral: FluidStorageAPI, target: string, slot: number, name: string): number | null {
        let rawPushedAmount = peripheral.pushFluid(target, this.fluidTransferLimit, name)
        if (rawPushedAmount == null) return 0
        if (rawPushedAmount != null) {
            return rawPushedAmount / 1000
        }
        return rawPushedAmount
    }

    list(peripheral: FluidStorageAPI): LuaTable<AnyNotNil, any> {
        let tanks = peripheral.tanks()
        if (tanks == null) return new LuaTable()
        // so, we need to copy amount value into a count value with (!) reduced count (?)
        for (const [_, tank] of tanks) {
            tank["count"] = tank.amount / 1000
        }
        return tanks
    }
}

export const DEFAULT_INVENTORY_STRATEGY = new InventoryStrategy()
export const DEFAULT_FLUID_STORAGE_STRATEGY = new FluidStorageStrategy()
