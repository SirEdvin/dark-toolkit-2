import { BaseAgent } from '../agent'
import { OperatorStrategy, DEFAULT_FLUID_STORAGE_STRATEGY, DEFAULT_INVENTORY_STRATEGY } from "./strategies"
import {
    InventoryAPI,
} from "typed-peripheral/apis/inventory";
import {
    FluidStorageAPI,
} from "typed-peripheral/apis/fluid_storage";
import { CollectionReport, SingleCollectionReport, TargetSelectionStrategy } from "cc-doerlib/extra/types"
import { NamePredicate } from "typed-peripheral/base"
import { KeyEvent } from "cc-corelib/event";
import { IPeripheralProvider } from "typed-peripheral/base";
import { queueLocalEvent } from "cc-corelib/bus";
import { CollectionReportEvent } from "./events";
import { NAME_DICTIONARY } from "cc-corelib/extra/dictionaries";


export abstract class AbstractCollector<T> extends BaseAgent {
    sourcePeripherals: Array<T>;
    sourcePredicate: NamePredicate;
    targetSelectionStrategy: TargetSelectionStrategy
    postReports: boolean
    postReportsPredicate: NamePredicate
    operatorStrategy: OperatorStrategy<T>

    constructor(
        name: string,
        loopDelay: number,
        operatorStrategy: OperatorStrategy<T>, 
        targetSelectionStrategy: TargetSelectionStrategy,
        sourcePredicate: NamePredicate = (name: string) => true,
        postReports: boolean = true,
        postReportsPredicate: NamePredicate = (name) => true
    ) {
        super(name, loopDelay);
        this.operatorStrategy = operatorStrategy
        this.targetSelectionStrategy = targetSelectionStrategy;
        this.sourcePeripherals = [];
        this.sourcePredicate = sourcePredicate;
        this.postReports = postReports
        this.postReportsPredicate = postReportsPredicate
    }

    protected configureInternal(): void {
        let inventories = this.operatorStrategy.peripheralProvider.collect();
        this.sourcePeripherals = inventories.filter(
            (inv) => this.sourcePredicate(peripheral.getName(inv))
        );
        this.log.info("Running collector", this.name);
        this.log.info("Discovered", this.sourcePeripherals.length, "sources")
        this.subscribeToEvent("key", (event: KeyEvent) => {
            if (event.key == keys.c) {
                this.collect()
            }
        })
    }

    protected collect(): void {
        let collectingMetrics: Map<string, number> = new Map();
        this.sourcePeripherals.forEach((source) => {
            let sourceList = this.operatorStrategy.list(source);
            if (sourceList != null) {
                for (const [slot, something] of sourceList) {
                    if (!NAME_DICTIONARY.has(something.name)) {
                        this.operatorStrategy.newNameHandler(source, something.name, slot as number)
                    }
                    let target = this.targetSelectionStrategy(something.name);
                    if (target != null) {
                        let pushedAmount = this.operatorStrategy.push(source, target, slot as number, something.name);
                        if (pushedAmount != null) {
                            if (!collectingMetrics.has(something.name)) {
                                collectingMetrics.set(something.name, 0)
                            }
                            collectingMetrics.set(something.name, collectingMetrics.get(something.name) + pushedAmount)
                        }
                    } else {
                        this.log.warn("Cannot find out where to put this item", something.name);
                    }
                }
            }
        });
        // building report
        if (this.postReports) {
            let reportEvent = new CollectionReportEvent(
                this.name, new CollectionReport(
                    Array.from(collectingMetrics.entries()).filter((value) => this.postReportsPredicate(value[0])).map((value) => {
                        return new SingleCollectionReport(value[0], value[1])
                    })
                )
            )
            this.log.debug("Publishing report event with", reportEvent.report.reports.length, "records")
            queueLocalEvent(reportEvent)
        }
    }

    protected perform(): void {
        this.collect()
    }
}

export class CollectorAgent extends AbstractCollector<InventoryAPI> {
    constructor(
        name: string,
        loopDelay: number,
        targetSelectionStrategy: TargetSelectionStrategy,
        sourcePredicate: NamePredicate = (name: string) => true,
        postReports: boolean = true,
        postReportsPredicate: NamePredicate = (name) => true
    ) {
        super(name, loopDelay, DEFAULT_INVENTORY_STRATEGY, targetSelectionStrategy, sourcePredicate, postReports, postReportsPredicate);
    }
}

export class FluidCollectorAgent extends AbstractCollector<FluidStorageAPI> {

    constructor(
        name: string,
        loopDelay: number,
        targetSelectionStrategy: TargetSelectionStrategy,
        sourcePredicate: NamePredicate = (name: string) => true,
        postReports: boolean = true,
        postReportsPredicate: NamePredicate = (name) => true
    ) {
        super(name, loopDelay, DEFAULT_FLUID_STORAGE_STRATEGY, targetSelectionStrategy, sourcePredicate, postReports, postReportsPredicate);
    }

}
