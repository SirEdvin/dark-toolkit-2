import { BaseAgent } from '../agent'
import {
    InventoryAPI,
} from "typed-peripheral/apis/inventory";
import {
    FluidStorageAPI,
} from "typed-peripheral/apis/fluid_storage";
import { OperatorStrategy, DEFAULT_FLUID_STORAGE_STRATEGY, DEFAULT_INVENTORY_STRATEGY } from "./strategies"
import { NamePredicate } from "typed-peripheral/base"

export abstract class AbstractSpreaderAgent<T> extends BaseAgent {
    sourcePeripheralName: string
    targetPredicate: NamePredicate;
    targetPeripherals: Array<T>
    sourcePeripheral: T
    operatorStrategy: OperatorStrategy<T>

    constructor(
        name: string,
        loopDelay: number,
        sourcePeripheralName: string,
        operatorStrategy: OperatorStrategy<T>, 
        targetPredicate: NamePredicate = (name: string) => true
    ) {
        super(name, loopDelay);
        this.operatorStrategy = operatorStrategy
        this.sourcePeripheralName = sourcePeripheralName
        this.targetPredicate = targetPredicate
        this.targetPeripherals = []
    }

    protected configureInternal(): void {
        let inventories = this.operatorStrategy.peripheralProvider.collect();
        this.targetPeripherals = inventories.filter(
            (inv) =>
                this.targetPredicate(peripheral.getName(inv))
        );
        this.sourcePeripheral = this.operatorStrategy.peripheralProvider.wrap(this.sourcePeripheralName)  
        if (this.sourcePeripheral == null)
            throw "Cannot find source peripheral, so it is abort";
    }

    protected perform(): void {
        let elements = this.operatorStrategy.list(this.sourcePeripheral)
        if (elements != null) {
            for (const [slot, element] of elements) {
                let pushedCount = 0
                for (let targetInventory of this.targetPeripherals) {
                    let currentPushedCount = this.operatorStrategy.push(this.sourcePeripheral, peripheral.getName(targetInventory), slot as number, element.name)
                    if (currentPushedCount != null) {
                        pushedCount += currentPushedCount
                    } else {
                        this.log.warn("Somehow pushed count is nil?")
                    }
                    if (pushedCount >= element.count) {
                        break
                    }
                }
            }
        }
    }
}

export class SpreaderAgent extends AbstractSpreaderAgent<InventoryAPI> {
    constructor(
        name: string,
        loopDelay: number,
        sourcePeripheralName: string,
        targetPredicate: NamePredicate = (name: string) => true
    ) {
        super(name, loopDelay, sourcePeripheralName, DEFAULT_INVENTORY_STRATEGY, targetPredicate);
    }
}

export class FluidSpreaderAgent extends AbstractSpreaderAgent<FluidStorageAPI> {
    constructor(
        name: string,
        loopDelay: number,
        sourcePeripheralName: string,
        targetPredicate: NamePredicate = (name: string) => true
    ) {
        super(name, loopDelay, sourcePeripheralName, DEFAULT_FLUID_STORAGE_STRATEGY, targetPredicate);
    }
}