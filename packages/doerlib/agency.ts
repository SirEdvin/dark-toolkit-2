import * as bus from 'cc-corelib/bus'
import * as logging from "cc-corelib/logging"
import * as events from 'cc-corelib/event'
import * as fetcher from "cc-corelib/fetcher";
import * as pretty from "cc.pretty";

import {AgentState, BaseAgent} from './agent'

const _log = logging.create("agency")

events.eventInitializers.push()

export const AGENCY_CONTROL_EVENT = "__agency_control_event__"

export enum AgencyControlEventType {
    REBOOT, UPDATE, STOP
}

export enum AgencyType {
    SLAVE = 1,
    CONTROLLER = 10,
    SUPERVISOR = 100
}

export enum AgencyState {
    INITIAL, WORKING, UPDATING, REBOOTING, STOPPING, STOPPED
}


export class AgencyControlEvent implements events.IEvent {

    constructor(readonly control_type: AgencyControlEventType) {}

    get_name(): string {
        return AGENCY_CONTROL_EVENT
    }
    get_args(): any[] {
        return [this.control_type]
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != AGENCY_CONTROL_EVENT) return null;
        let ev = new AgencyControlEvent(args[1]);
        return ev;
    }
}

events.eventInitializers.push(AgencyControlEvent.init)

export class AgencyInformation {
    name: string
    agents: object
    state: AgencyState
    networkName: string
    type: AgencyType
}

// TODO: add agency state broadcasting

export type CustomTask = () => void

export class Agency {
    agents: Array<BaseAgent>
    type: AgencyType
    name: string
    bus: bus.EventBus
    state: number
    update_loop_delay: number
    waiting_delay: number
    customTasks: CustomTask[]
    stopCalls: CustomTask[]

    constructor(type: AgencyType = AgencyType.CONTROLLER, name: string | null = null, update_loop_delay: number = 5, waiting_delay: number = 1) {
        this.type = type
        this.name = name || os.getComputerLabel()
        this.update_loop_delay = update_loop_delay
        this.state = AgencyState.INITIAL
        this.agents = []
        this.bus = new bus.EventBus(this.name)
        this.waiting_delay = waiting_delay
        this.customTasks = []
        this.stopCalls = []
    }

    addAgent(agent: BaseAgent) {
        this.agents.push(agent)
        agent.configure(this.name)
        if (agent.targetEvents != null) {
            agent.targetEvents.forEach((value) => this.bus.subscribe(value))
        }
    }

    addCustomTask(task: CustomTask, stopCall: CustomTask) {
        this.customTasks.push(task)
        this.stopCalls.push(stopCall)
    }

    configure(...events: Array<string>) {
        events.forEach((value) => this.bus.subscribe(value))
        this.bus.subscribe(AGENCY_CONTROL_EVENT)
    }

    run() {
        _log.info("Starting agency", this.name)
        this.state = AgencyState.WORKING
        const tasks = []
        tasks.push(() => this.bus.run())
        tasks.push(() => this.controlEventProcessor())
        tasks.push(() => this.keyEventProcessor())
        this.customTasks.forEach((task) => tasks.push(task))
        this.agents.forEach((agent) => tasks.push(() => agent.run()))
        parallel.waitForAll(...tasks)
        _log.warn("Fully stopped agency")
        if (this.state == AgencyState.STOPPING) {
            this.state = AgencyState.STOPPED
        }
    }

    private update() {
        fetcher.installEverything()
        this.scheduleReboot()
    }
    private stop() {
        this.stopCalls.forEach((call) => call())
        this.stopAgents(true)
        this.stopBus()
    }

    private controlEventProcessor() {
        while (this.state == AgencyState.WORKING) {
            const event = events.pullEventAs(AgencyControlEvent, AGENCY_CONTROL_EVENT)
            if (event.control_type == AgencyControlEventType.REBOOT) {
                this.state = AgencyState.REBOOTING
                this.stop()
                os.reboot()
            } else if (event.control_type == AgencyControlEventType.STOP) {
                this.stop()
                break
            } else if (event.control_type == AgencyControlEventType.UPDATE) {
                this.update()
            }
        }
        _log.info("Agency control event processor stopped")
    }

    private keyEventProcessor() {
        while (this.state == AgencyState.WORKING) {
            const event = events.pullEventAs(events.KeyEvent, "key")
            if (event.key == keys.q) {
                this.scheduleStop()
            } else if (event.key == keys.r) {
                this.scheduleReboot()
            } else if (event.key == keys.u) {
                this.update()
            } else if (event.key == keys.e) {
                _log.info("Agency subscribed to this events:")
                this.bus.target_events.forEach((name) => {
                    _log.info("\t", name)
                })
            } else if (event.key == keys.d) {
                if (!this.bus.debug) {
                    _log.warn("Enabling event debugger")
                    this.bus.debug = true
                } else {
                    _log.warn("Disable event debugger")
                    this.bus.debug = false
                }
            }
        }
    }

    stopAgents(wait_for_stop: boolean = false) {
        this.agents.forEach((agent) => agent.stop())
        if (wait_for_stop) {
            let all_agents_stopped = false
            while (!all_agents_stopped) {
                all_agents_stopped = true
                let runningAgents = []
                this.agents.forEach((agent) => {
                    if (agent.state != AgentState.STOPPED) {
                        all_agents_stopped = false
                        runningAgents.push(agent.name)
                    }
                })
                if (!all_agents_stopped) {
                    _log.info("Waiting for agents to stop:", runningAgents.join(", "))
                    os.sleep(this.waiting_delay)
                }
            }
        }
    }

    stopBus() {
        const unstoppedAgent = this.agents.find((agent) => agent.state != AgentState.STOPPED)
        if (unstoppedAgent != undefined) {
            _log.warn("Cannot stop bus in this condition, agent", unstoppedAgent.name, "is not stopped yet")
        } else {
            this.bus.stop()
        }
    }

    private scheduleStop() {
        this.state = AgencyState.STOPPING
        this.bus.queueLocalEvent(new AgencyControlEvent(AgencyControlEventType.STOP))
    }

    private scheduleReboot() {
        this.bus.queueLocalEvent(new AgencyControlEvent(AgencyControlEventType.REBOOT))
    }
}