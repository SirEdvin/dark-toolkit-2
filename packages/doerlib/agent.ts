import * as logging from 'cc-corelib/logging'
import * as events from 'cc-corelib/event'

const _log = logging.create("doerlib.agent")

export const AGENT_STOP_EVENT = "__agent_stop_event__"

export enum AgentState {
    INITIAL, WORKING, SHUTTING_DOWN, STOPPED
}

export class AgentStopEvent implements events.IEvent {

    get_name(): string {
        return AGENT_STOP_EVENT
    }
    get_args(): any[] {
        return []
    }

    public static init(args: any[]): events.IEvent | null {
        if (!(typeof args[0] === "string") || (args[0] as string) != AGENT_STOP_EVENT) return null;
        let ev = new AgentStopEvent();
        return ev;
    }
}

events.eventInitializers.push(AgentStopEvent.init)


export abstract class BaseAgent {
    name: string
    ownerName: string
    loopDelay: number | null
    state: number = AgentState.INITIAL
    configured: boolean = false
    targetEvents: Array<string>
    handlers: Map<string, (event: events.IEvent) => void>
    log: logging.Logger

    constructor(name: string | null, loopDelay: number = 20) {
        this.name = name || os.getComputerLabel()
        this.loopDelay = loopDelay
        this.ownerName = ""
        this.targetEvents = []
        this.handlers = new Map()
        this.log = logging.create(this.name)
    }

    configure(owner_name: string) {
        this.ownerName = owner_name
        this.configureInternal()
        this.configured = true
    }

    protected abstract configureInternal(): void;
    protected abstract perform(): void;

    private performLoop(delay: number): void {
        while (this.state == AgentState.WORKING) {
            this.perform()
            os.sleep(delay)
        }
    }

    private eventListener(): void {
        const event_filter = new Map<string, boolean>()
        this.targetEvents.forEach((value) => {
            event_filter[value] = true
        })
        while (this.state == AgentState.WORKING) {
            let new_event = events.pullEvent()
            if (new_event instanceof AgentStopEvent) {
                break
            }
            if (new_event != null && event_filter[new_event.get_name()] && this.handlers[new_event.get_name()] != null) {
                this.handlers[new_event.get_name()](new_event)
            }
        }
    }

    subscribeToEvent<Type extends events.IEvent>(name: string, handler: (event: Type) => void) {
        this.targetEvents.push(name)
        this.handlers[name] = handler
    }

    start(): void {
        if (!this.configured) {
            throw "Agent " + this.name + " is not configured"
        }
        _log.info("Starting agent", this.name)
    }

    run() {
        let tasks = []
        if (this.loopDelay != null && this.loopDelay > 0) {
            tasks.push(() => this.performLoop(this.loopDelay))
        }
        if (this.targetEvents != null) {
            tasks.push(() => this.eventListener())
        }
        this.state = AgentState.WORKING
        parallel.waitForAll(...tasks)
        this.state = AgentState.STOPPED
        _log.info("Agent", this.name, "stopped")
    }

    stop() {
        this.state = AgentState.SHUTTING_DOWN
        os.queueEvent(AGENT_STOP_EVENT)
        _log.warn("Stopping agent", this.name, "wait for loop finish, please")
    }

    cleanup() {
        this.state = AgentState.STOPPED
    }
}