export function withTermColor<Type>(color: number, func: () => Type): Type {
    let currentColor = term.getTextColor()
    term.setTextColor(color)
    let funcResult = func()
    term.setTextColor(currentColor)
    return funcResult
}

export function writeError(err: string): void {
    withTermColor(colors.red, () => term.write(err))
}