export function searchItem(name: string): number {
    for (const i of $range(1, 16)) {
        let itemDetail = turtle.getItemDetail(i);
        if (itemDetail != null && itemDetail["name"] == name) {
            return i
        }
    }
    return -1;
}

export function hasFreeSlot(): boolean {
    for (const i of $range(1, 16)) {
        let itemDetail = turtle.getItemDetail(i);
        if (itemDetail == null) {
            return true
        }
    }
    return false
}

export function hasSpaceForItem(name: string, amount: number = 1): boolean {
    for (const i of $range(1, 16)) {
        let itemDetail = turtle.getItemDetail(i);
        if (itemDetail == null || (itemDetail["name"] == name && itemDetail["count"] + amount < 64)) {
            return true
        }
    }
    return false
}