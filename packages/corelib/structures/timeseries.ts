export class TimeseriesRecord<T> {
    /**
     * 
     * @param value Value of time record
     * @param timestamp Timestamp, expected to be taked from `os.epoch` and be in milliseconds
     */
    constructor(readonly value: T, readonly timestamp: number = os.epoch("utc")) {}
}

export class Timeseries<T> {
    values: Array<TimeseriesRecord<T>>
    constructor(readonly limit: number) {
        this.values = []
    }

    /**
     * Frame of time in milliseconds usually
     */
    public get timeframe(): number {
        return this.values[this.values.length - 1].timestamp - this.values[0].timestamp
    }

    public append(record: TimeseriesRecord<T>) {
        this.values.push(record)
        if (this.values.length > this.limit) {
            this.values.shift()
        }
    }
}