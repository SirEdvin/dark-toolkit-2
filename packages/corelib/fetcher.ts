import {Result, Ok, Err} from './monads/result';
import * as logging from './logging'

const CURRENT_VERSION_FILE = "/dark_toolkit/versions.lua"
const DEFAULT_CURRENT_VERSION = "1990-09-02-08-20-08"
export const LITTLE_HAND_URL_SETTINGS = "dark_toolkit.little_hand_url"
const LITTLE_HAND_URL = settings.get(LITTLE_HAND_URL_SETTINGS, "https://d1.siredvin.site/");
export const GROUP_SETTING = "dark_toolkit.group"
export const EXTRA_PROGRAMS_SETTING = "dark_toolkit.extra_programs"
const DEFAULT_PROGRAMS = ["dark", "ddebug"]

const _log = logging.create("fetcher")

function httpGet(url: string): Result<any, string> {
    let response = http.get(url)
    if (!response[0]) { return Err(response[1] as string) }
    let parsed_response = response[0] as HTTPResponse
    let content = parsed_response.readAll()
    parsed_response.close()
    return Ok(content)
}

function httpGetJson(url: string): Result<any, string> {
    return httpGet(url).map(content => textutils.unserialiseJSON(content));
}

function buildInfoUrl(group: string, entity: string): string {
    return LITTLE_HAND_URL + "/storage/" + group + "/" + entity + "/info"
}

function buildLatestUrl(group: string, entity: string): string {
    return LITTLE_HAND_URL + "/storage/" + group + "/" + entity + "/latest"
}

function writeFile(path: string, content: any): Result<boolean, string> {
    let f = fs.open(path, "w")
    if (f[0] == null) {
        return Err(f[1] as string)
    }
    let handler: FileHandle = f[0];
    handler.write(content);
    handler.close()
    return Ok(true)
}

function ensureDirectoryExists(path: string) {
    if (!fs.exists(path)){
        fs.makeDir(path)
    }
}

function getLocalCurrentVersions(): {[key: string]: {[key:string]: string}} {
    let f = fs.open(CURRENT_VERSION_FILE, "r")
    if (f[0] == null) {
        return {}
    }
    let handler: FileHandle = f[0];
    let data: {[key: string]: {[key:string]: string}} = textutils.unserialise(handler.readAll())
    handler.close()
    return data || {}
}

function setLocalCurrentVersions(data: {[key: string]: {[key:string]: string}}) {
    let f = fs.open(CURRENT_VERSION_FILE, "w")
    if (f[0] == null) {
        return {}
    }
    let handler: FileHandle = f[0];
    handler.write(textutils.serialise(data))
}

function getLocalCurrentVersionInfo(group: string, entity: string): string {
    let data = getLocalCurrentVersions()
    let groupInfo = data[group]
    if (groupInfo == null) {
        return DEFAULT_CURRENT_VERSION
    }
    return groupInfo[entity] || DEFAULT_CURRENT_VERSION
}

function setLocalCurrentVersionInfo(group: string, entity: string, version: string) {
    let data = getLocalCurrentVersions()
    let groupInfo = data[group]
    if (groupInfo == null) {
        data[group] = {entity: version}
    } else {
        data[group][entity] = version
    }
    setLocalCurrentVersions(data)
}


export function downloadFile(group: string, entity: string, path_to_store: string = "/dark_toolkit/main.lua") {
    const current_version = getLocalCurrentVersionInfo(group, entity)
    let version_info = httpGetJson(buildInfoUrl(group, entity))
    if (version_info.isErr()) { 
        _log.error("There is an error when fetching version info for ", group, ":", entity, ", ", version_info.unwrapErr())
    } else {
        let latest_version = version_info.unwrap().latest
        if (latest_version > current_version) {
            _log.info("Find newer version", latest_version, "that current one", current_version, "performing update")
            let latest_file = httpGet(buildLatestUrl(group, entity));
            if (latest_file.isErr()) {
                _log.error("There is an error when fetching latest file for ", group, ":", entity, ", ", latest_file.unwrapErr())
            } else {
                let file_data = latest_file.unwrap()
                let writeResult = writeFile(path_to_store, file_data)
                if (writeResult.isErr()) {
                    _log.error("There is an error when writing latest file for", group, ":", entity, ", ", writeResult.unwrapErr())
                } else {
                    setLocalCurrentVersionInfo(group, entity, latest_version)
                    settings.save()
                    _log.info("Downloading was successful, file stored at", path_to_store)
                }
            }
        } else {
            _log.info("For file", group, ":", entity , "latest version", latest_version, "is not newer that current one", current_version)
        }
    }
}


export function installEverything() {
    ensureDirectoryExists("/dark_toolkit")
    ensureDirectoryExists("/dark_toolkit/programs")
    ensureDirectoryExists("/startup")

    downloadFile("system", "startup", "/startup/10_dark_toolkit.lua")

    let computerLabel = os.getComputerLabel()
    if (computerLabel != null && computerLabel.length > 0) {
        let sanitizedLabel = computerLabel.split(":")[0];
        let group = settings.get(GROUP_SETTING, "dev")
        downloadFile(group, sanitizedLabel)
    }

    DEFAULT_PROGRAMS.forEach((pr) => downloadFile("programs", pr, "/dark_toolkit/programs/" + pr + ".lua"))

    let extraPrograms: string = settings.get(EXTRA_PROGRAMS_SETTING, "")
    extraPrograms.split(",").forEach((pr) => {
        if (pr.trim().length > 0)
            downloadFile("programs", pr, "/dark_toolkit/programs/" + pr + ".lua")
    })
}