import * as logging from "../logging"


const _log = logging.create("dictionaries")

export class PersistentDictionary<T1, T2> {
    table: LuaTable<T1, T2>
    fileName: string
    constructor(fileName: string = "some_dictionary.db") {
        this.fileName = fileName
        let loadedTable = this.load()
        if (loadedTable != null) {
            this.table = loadedTable
        } else {
            this.table = new LuaTable()
        }
    }

    protected save() {
        let f = fs.open(this.fileName, "w")
        if (f[0] == null) {
            _log.error("Cannot open file to save data")
        } else {
            let handler = f[0] as FileHandle
            handler.write(textutils.serialise(this.table))
            handler.close()
        }
    }

    protected load(): LuaTable<T1, T2> | null {
        if (fs.exists(this.fileName)) {
            let f = fs.open(this.fileName, "r")
            if (f[0] == null) {
                _log.error("Cannot open file to load data")
            } else {
                let handler = f[0] as FileHandle
                let data = handler.readAll()
                let loadedTable = textutils.unserialise(data)
                handler.close()
                return loadedTable
            }
        }
        return null
    }

    public has(name: T1): boolean {
        return this.table.has(name)
    }

    public get(name: T1, defaultValue: T2 | null = null): T2 | null{
        const value = this.table.get(name)
        if (value == null)
            return defaultValue
        return value
    }

    public set(name: T1, value: T2) {
        this.table.set(name, value)
        this.save()
    }

}


export class NameDictionary extends PersistentDictionary<string, string> {
    constructor(fileName: string = "name_dictionary.db") {
        super(fileName)
    }

    public track(name: string, value: string) {
        if (!this.has(name))
            this.set(name, value)
    }
}

export const NAME_DICTIONARY = new NameDictionary()
