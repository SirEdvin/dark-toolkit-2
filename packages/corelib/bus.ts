import * as logging from './logging'
import * as events from './event'

const _log = logging.create("bus")
const _PROTOCOL_NAME = "ScaryBus"
const _PROTOCOL_VERSION = "v1"
const _PROTOCOL = _PROTOCOL_NAME + _PROTOCOL_VERSION

export const LOCAL_DESTINATION = "__local__"
export const BROADCAST_DESTINATION = "__broadcast__"

class ProtocolMessage {
    constructor(readonly name: string, readonly args: any[], readonly source: string, readonly timestamp: number = os.epoch("utc")) {}
}


export class EventBus {
    name: string
    debug: boolean
    target_events: Set<string>
    protocol: string
    timeout: number
    enabled: boolean = false
    processors: Array<(arg0: ProtocolMessage) => void>

    constructor(name: string | null, debug: boolean = false, protocol: string = _PROTOCOL, timeout: number = 20) {
        this.name = name || os.getComputerLabel()
        this.debug = debug
        this.target_events = new Set()
        this.protocol = protocol
        this.timeout = timeout
        this.processors = []
    }

    subscribe(event: string) {
        this.target_events.add(event)
    }

    unsubscribe(event: string) {
        this.target_events.delete(event)
    }

    hook(processor: (arg0: ProtocolMessage) => void) {
        this.processors.push(processor)
    }

    queueLocalEvent(event: events.IEvent) {
        os.queueEvent(event.get_name(), ...event.get_args())
    }

    broadcastEvent(event: events.IEvent) {
        if (this.debug) {
            _log.info("Broadcasting event", event.get_name())
        }
        if (this.target_events.has(event.get_name())) {
            this.queueLocalEvent(event)
        }
        rednet.broadcast(new ProtocolMessage(event.get_name(), event.get_args(), this.name), this.protocol)
    }

    queueEvent(destination: string, event: events.IEvent, silent: boolean = false) {
        if (destination === LOCAL_DESTINATION) {
            this.queueLocalEvent(event)
        } else if (destination === BROADCAST_DESTINATION) {
            this.broadcastEvent(event)
        } else {
            const locationID = rednet.lookup(this.protocol, destination)
            if (this.debug) {
                _log.info("Sending event", event.get_name(), "to", locationID)
            }
            if (locationID != null) {
                rednet.send(locationID, new ProtocolMessage(event.get_name(), event.get_args(), this.name), this.protocol)
            } else {
                if (!silent) {
                    _log.warn("Cannot find destination", destination)
                }
            }
        }
    }

    private processMessage(message: ProtocolMessage) {
        if (this.debug) {
            _log.info("Received message", message.name, ...message.args)
        }
        if (this.target_events.has(message.name)) {
            this.processors.forEach((processor) => processor(message))
            os.queueEvent(message.name, ...message.args)
        }
    }

    run() {
        _G.BUS = this
        const modem: ModemPeripheral | null = peripheral.find("modem", (_, modem) => (modem as ModemPeripheral).isWireless())[0] as ModemPeripheral
        if (modem == null) {
            _log.warn("No modem, skip remote processing for event bus")
        } else {
            const modem_name = peripheral.getName(modem)
            if (!rednet.isOpen(modem_name)) {
                rednet.open(modem_name)
            }
            rednet.host(this.protocol, this.name)
            _log.info("Hosting protocol", this.protocol, "as", this.name)
            this.enabled = true
            while (this.enabled) {
                const values = rednet.receive(this.protocol, this.timeout)
                if (values[0] !== null) {
                    this.processMessage(values[1])
                }
            }
        }
    }

    stop () {
        this.enabled = false
    }
}


export function queueEvent(destination: string, event: events.IEvent, silent: boolean = false) {
    _G.BUS.queueEvent(destination, event, silent)
}

export function broadcastEvent(event: events.IEvent) {
    _G.BUS.broadcastEvent(event)
}

export function queueLocalEvent(event: events.IEvent) {
    _G.BUS.queueLocalEvent(event)
}
