export class Subcomand {
    constructor(readonly name: string, readonly usage: string, readonly flags: {[key: string]: string}) {}
}

export class CLIApplication {
    constructor (readonly name: string, readonly subcomands: Array<Subcomand>) {
        let self = this
        subcomands.forEach((com) => {
            let defined_function = self[com.name]
            if (defined_function == null) {
                throw "Subcommand " + com.name + " is not defined for cli application " + self.name
            }
        })
    }
    
    private printUsage() {
        print("You can use next following commands with program", this.name, ":")
        this.subcomands.forEach((com) => print("    ", com.usage))
    }

    run(...args: string[]) {
        let [selectedCommand, ...extraArguments] = args;
        if (selectedCommand == null) {
            this.printUsage()
        } else {
            let command = this.subcomands.find((com) => com.name == selectedCommand)
            if (command == null) {
                this.printUsage()
            } else {
                let flags: {[key: string]: boolean} = {}
                let args: Array<String> = [];
                extraArguments.forEach((arg) => {
                    if (arg.startsWith("-")) {
                        let flag_name = command.flags[arg]
                        if (flag_name == null) {
                            throw "Unknown flag  " + arg + " for command " + command.name
                        }
                        flags[flag_name] = true;
                    } else {
                        args.push(arg)
                    }
                })
                this[command.name](args, flags)
            }
        }
    }
}