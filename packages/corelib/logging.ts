import { withTermColor } from "./utils";

export class LoggingColors {
    debug: number = colors.gray;
    info: number = colors.white;
    warn: number = colors.yellow;
    error: number = colors.red;
    default: number = colors.white;
    success: number = colors.green;
}

export enum LoggingLevel {
    DEBUG = 0,
    INFO = 10,
    SUCCESS = 15,
    WARN = 20,
    ERROR = 30,
}

export class Logger {
    name: string;
    dateFormat: string;
    format: string;
    level: LoggingLevel;
    colors: LoggingColors;

    constructor(
        name: string = "",
        level: LoggingLevel | null = null,
        dateFormat: string = "%X",
        format: string = "[%date%] <%name%> [%level%] %log%",
        colors: LoggingColors | null = null
    ) {
        this.name = name;
        this.dateFormat = dateFormat;
        this.format = format;
        if (level == null) {
            let defaultLevel = settings.get("dark_toolkit.logging_level", "info") as string
            this.level = LoggingLevel[defaultLevel.toUpperCase()]
        } else {
            this.level = level;
        }
        if (colors != null) {
            this.colors = colors;
        } else {
            this.colors = new LoggingColors();
        }
    }

    formatDate(): string {
        return os.date(this.dateFormat) as string;
    }

    formatTemplate(level: LoggingLevel, log: string): string {
        return this.format
            .replace("%date%", this.formatDate())
            .replace("%name%", this.name)
            .replace("%level%", LoggingLevel[level])
            .replace("%log%", log);
    }

    serialize(args: any[]): string {
        return args.map((x) => x.toString()).join(" ");
    }

    log(level: LoggingLevel, ...args: any[]) {
        if (level >= this.level) {
            let color =
                this.colors[LoggingLevel[level].toLowerCase()] ||
                this.colors.default;
            withTermColor(color, () =>
                print(this.formatTemplate(level, this.serialize(args)))
            );
        }
    }

    debug(...args: any[]) {
        this.log(LoggingLevel.DEBUG, ...args);
    }

    info(...args: any[]) {
        this.log(LoggingLevel.INFO, ...args);
    }

    warn(...args: any[]) {
        this.log(LoggingLevel.WARN, ...args);
    }

    error(...args: any[]) {
        this.log(LoggingLevel.ERROR, ...args);
    }

    success(...args: any[]) {
        this.log(LoggingLevel.SUCCESS, ...args);
    }
}

export function create(
    name: string,
    level: LoggingLevel | null = null
): Logger {
    return new Logger((name = name), (level = level));
}
