import { kvStoragePeripheralProvider } from "typed-peripheral/datafortress/kvStorage"
import { Step, Test } from "soteria"
import * as logging from "cc-corelib/logging"

const _log = logging.create("test")

const kvStorage = kvStoragePeripheralProvider.findOrThrow()
const baseKey = "testKey"

_log.info("Starting kvstorage test")

kvStorage.put(baseKey, "1")
const v1 = kvStorage.get(baseKey)
if (v1 != "1") throw "On first access value is not 1"

kvStorage.put(baseKey, "2")
const v2 = kvStorage.get(baseKey)
if (v2 != "2") throw "On second access value is not 2"

kvStorage.delete(baseKey)
const v3 = kvStorage.get(baseKey)
if (v3 != null) throw "On second access value is not null"

_log.info("Test finished")
