import * as logging from "cc-corelib/logging"
import { fluidStoragePeripheralProvider } from "typed-peripheral/apis/fluid_storage"
import * as pretty from "cc.pretty";

const _log = logging.create("test")

_log.info("Starting chest transfer test")

function testInventory(): void {
    const rightFluidStorage = fluidStoragePeripheralProvider.wrap("right")
    const leftFluidStorage = fluidStoragePeripheralProvider.wrap("left")
    const firstResult = rightFluidStorage.pushFluid("left")
    if (firstResult != 1000) throw "Transfer from right to left is not 1000"
    const secondResult = leftFluidStorage.pushFluid("right")
    if (secondResult != 1000) throw "Transfer from right to left is not 1000"
    const thridResult = leftFluidStorage.pullFluid("right")
    if (thridResult != 1000) throw "Pull from right to left is not 1000"
    const fourthResult = rightFluidStorage.pullFluid("left")
    if (fourthResult != 1000) throw "Pull from right to left is not 1000"
}

function eventListener(): void {
    while (true) {
        let event_data = os.pullEvent()
        if (event_data[0].startsWith("peripheral"))
            pretty.pretty_print(event_data)
    }
}


parallel.waitForAny(
    testInventory, eventListener
)


_log.info("Test finished")