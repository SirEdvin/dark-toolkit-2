import * as logging from "cc-corelib/logging"
import { inventoryPeripheralProvider } from "typed-peripheral/apis/inventory"
import * as pretty from "cc.pretty";

const _log = logging.create("test")

_log.info("Starting chest 1 test")

function testInventory(cycles: number = 100): void {
    const inventory = inventoryPeripheralProvider.findOrThrow()
    for (let i = 0; i < cycles; i++) {
        let result = inventory.getItemDetail(1)
        if (result == null) {
            throw "Null detected on " + i.toString() + " run"
        }
    }
    print("No null detected")
}

function eventListener(): void {
    while (true) {
        let event_data = os.pullEvent()
        if (event_data[0].startsWith("peripheral"))
            pretty.pretty_print(event_data)
    }
}


parallel.waitForAny(
    testInventory, eventListener
)


_log.info("Test finished")