import * as logging from "cc-corelib/logging"
import { inventoryPeripheralProvider } from "typed-peripheral/apis/inventory"
import * as pretty from "cc.pretty";

const _log = logging.create("test")

_log.info("Starting chest transfer test")

function testInventory(): void {
    const rightInventory = inventoryPeripheralProvider.wrap("right")
    const leftInventory = inventoryPeripheralProvider.wrap("left")
    const firstResult = rightInventory.pushItems("left", 1, 1, 1)
    if (firstResult != 1) throw "Transfer from right to left is not 1"
    const secondResult = leftInventory.pushItems("right", 1, 1, 1)
    if (secondResult != 1) throw "Transfer from right to left is not 1"
    const thridResult = leftInventory.pullItems("right", 1, 1, 1)
    if (thridResult != 1) throw "Pull from right to left is not 1"
    const fourthResult = rightInventory.pullItems("left", 1, 1, 1)
    if (fourthResult != 1) throw "Pull from right to left is not 1"
}

function eventListener(): void {
    while (true) {
        let event_data = os.pullEvent()
        if (event_data[0].startsWith("peripheral"))
            pretty.pretty_print(event_data)
    }
}


parallel.waitForAny(
    testInventory, eventListener
)


_log.info("Test finished")